import esES from 'antd/lib/locale-provider/es_ES';
import appLocaleData from 'react-intl/locale-data/es';

export default {
    antd: esES,
    locale: 'es-ES',
    data: appLocaleData,
};
