import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import {Breadcrumb} from 'antd';
import T from "i18n-react/dist/i18n-react";


export default class LandraBreadcrumb extends Component {


    createBreadcrumbFragments = (position)=> {

        let html = [];

        for (let name in position) {
            if (position[name] === false) {
                html.push(
                    <Breadcrumb.Item key={name}>{T.translate("menu_" + name)}</Breadcrumb.Item>
                );
            } else if (position[name] === true) {
                html.push(
                    <Breadcrumb.Item key={name}
                                     style={{color: '#457c67'}}>{T.translate("menu_" + name)}</Breadcrumb.Item>
                );
            } else {
                html.push(
                    <Breadcrumb.Item key={name}><Link
                        to={position[name]}>{T.translate("menu_" + name)}</Link></Breadcrumb.Item>
                );
            }
        }
        ;

        return html;
    };

    render() {
        return (
            <div>
                <Breadcrumb separator={"|"}>
                    {this.createBreadcrumbFragments(this.props.position)}
                </Breadcrumb>
            </div>
        );
    };

};
