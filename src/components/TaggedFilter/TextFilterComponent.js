import React, { Component } from 'react';
import { Input, Icon } from 'antd';

const InputGroup = Input.Group;

export default class TextFilterComponent extends Component {


    render() {

        return (
            <InputGroup compact className='filterComponent' {...this.props}>
                <Input id={this.props.filterId} value={this.props.value} onPressEnter={this.props.onPressEnter} onChange={(e) => this.props.onChange(e.target.value, this.props.elmkey)} addonBefore={this.props.label} addonAfter={<Icon type="close" onClick={this.props.onCloseClick} />} />
            </InputGroup>
        )
    }
}