import React, { Component } from 'react';
import { Select, Input, Icon } from 'antd';

const InputGroup = Input.Group;
const Option = Select.Option;

export default class BooleanFilterComponent extends Component {



    render() {
        return (
            <InputGroup compact className='filterComponent filterComponentRange' {...this.props}>
                <span style={{
                    lineHeight: '30px',
                    padding: '0 5px',
                    textAlign: 'center',
                    border: '1px solid #d9d9d9',
                    borderRight: 'none',
                    backgroundColor: '#fafafa'
                }}>{this.props.label}</span>
                <Select value={this.props.value} defaultValue="" onChange={(status) => this.props.onChange(status, this.props.elmkey)}>
                    <Option value="">&nbsp;</Option>
                    <Option value="true">Si</Option>
                    <Option value="false">No</Option>
                </Select>
                <Icon type="close" style={{
                    lineHeight: '30px',
                    width: 28,
                    border: '1px solid #d9d9d9',
                    backgroundColor: '#fafafa'
                }} onClick={this.props.onCloseClick} />
            </InputGroup>
        )
    }
}