import React, { Component } from 'react';
import { Input, Icon } from 'antd';
import NumericInput from '../../common/Fields/NumericInput';

const InputGroup = Input.Group;

export default class NumberFilterComponent extends Component {


    render() {

        return (
            <InputGroup compact className='filterComponent' {...this.props}>
                <NumericInput value={this.props.value}
                              onPressEnter={this.props.onPressEnter}
                              onChange={(value) => this.props.onChange(value, this.props.elmkey)}
                              addonBefore={this.props.label}
                              addonAfter={<Icon type="close"
                                                onClick={this.props.onCloseClick} />} />
            </InputGroup>
        )
    }
}