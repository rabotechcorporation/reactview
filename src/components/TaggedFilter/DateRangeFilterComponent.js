import React, { Component } from 'react';
import { Input, DatePicker } from 'antd';
import moment from 'moment';

const RangePicker = DatePicker.RangePicker;

const InputGroup = Input.Group;

export default class DateRangeFilterComponent extends Component {


    parseValue = (value) => {
        for(var idx in value){
            value[idx] = moment(value[idx]);
        }
        return value;
    }

    render() {
        return (
            <InputGroup compact
                className='filterComponent filterComponentDateRange'
                {...this.props}>

                <RangePicker
                    style={{ width: 300 }}
                    ranges={{ "Hoy": [moment().startOf('day'), moment().endOf('day')], 'Esta Semana': [moment().startOf('week'), moment().endOf('week')], 'Este mes': [moment().startOf('month'), moment().endOf('month')] }}
                    showTime={{ format: 'HH:mm' }}
                    format="DD/MM/YYYY HH:mm"
                    value={this.parseValue(this.props.value)}
                    onPressEnter={this.props.onPressEnter}
                    placeholder={[this.props.label + ' Inicio', this.props.label + ' Fin']}
                    onChange={(dates, datestring) => { this.props.onChange(dates, this.props.elmkey, datestring, datestring); }}

                />

            </InputGroup>
        );
    }
}
