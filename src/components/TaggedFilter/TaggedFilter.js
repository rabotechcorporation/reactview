import React, {Component} from 'react';
import {Row, Col, Button, Popover, Switch} from 'antd';
import TextFilterComponent from './TextFilterComponent';
import RangeFilterComponent from './RangeFilterComponent';
import DateRangeFilterComponent from './DateRangeFilterComponent';
import DateFilterComponent from './DateFilterComponent';
import NumberFilterComponent from './NumberFilterComponent';
import BooleanFilterComponent from './BooleanFilterComponent';
import ComboFilterComponent from './ComboFilterComponent';
import T from "i18n-react/dist/i18n-react";

import './TaggedFilter.css';

export default class TaggedFilter extends Component {
    anyfilterOptions = [];
    state = {
        data: {}
    };

    componentDidMount = () => {
        if(this.props.columns){
            for (let idx in this.props.columns) {
                let elm = this.props.columns[idx];
                let visible = true;
                if (elm.filterOptional) {
                    visible = false
                }
                this.state[elm.key + "visible"] = visible;
            }
            this.forceUpdate();
        }
    }

    componentDidUpdate = (prevProps, prevState) => {
        if (prevProps.columns == undefined && this.props.columns || (this.props.columns && prevProps.columns && prevProps.columns.length !== this.props.columns.length)) {
            for (let idx in this.props.columns) {
                let elm = this.props.columns[idx];
                let visible = true;
                if (elm.filterOptional) {
                    visible = false
                }
                this.state[elm.key + "visible"] = visible;
            }
            this.forceUpdate();
        }
    };

    removeFilter = (key) => {
        let data = this.props.currentFilters;
        delete data[key];

        this.setState({data: data});
        if (this.props.filters) {
            this.props.filters(data);
        }
    };
    showFilter = (key, checked) => {
        let params = {};
        this.removeFilter(key);
        params[key + "visible"] = checked;

        this.setState(params);
    };
    textChange = (key, value, formattedValue) => {
        let data = this.props.currentFilters;

        data[key] = value;
        if (data[key] == "" || data[key] == undefined) {
            delete data[key];
        }

        if (formattedValue) {
            data[key + "formatted"] = formattedValue;
        }
        if (data[key + "formatted"] && (data[key + "formatted"][0] == "" || data[key + "formatted"][0] == undefined)) {
            delete data[key + "formatted"];
        }
        this.props.parent.forceUpdate();
        if (this.props.filters) {
            this.props.filters(data);
        }
    };
    drawFilters = (columns) => {
        let filters = [];
        this.filterOptions = [];
        for (let idx in columns) {
            let elm = columns[idx];
            if (elm.filter) {
                const switchOptions = {
                    checked: this.state[elm.key + "visible"] !== false,
                    style: {float: 'right'},
                    onChange: (checked) => this.showFilter(elm.key, checked)
                };

                this.filterOptions.push(
                    <div key={elm.key} style={{height: 30}}>
                        <b>{elm.specialLabel ? elm.specialLabel : elm.title}</b>
                            <Switch {...switchOptions} />
                        <br />
                    </div>
                );

                if (this.props.currentFilters[elm.key]) {
                    this.state[elm.key + "visible"] = true;
                }
                let inputOptions = {
                    onPressEnter: () => {
                        this.props.fetchFunction(this.props.currentFilters)
                    },
                    onChange: (value, key, formattedValue) => {
                        this.textChange(key ? key : elm.key, value, formattedValue)
                    },
                    value: this.props.currentFilters[elm.key],
                    label: elm.specialLabel ? elm.specialLabel : elm.title,
                    elmkey: elm.key,
                    key: elm.key,
                    onCloseClick: () => this.removeFilter(elm.key),
                    style: {
                        display: this.state[elm.key + "visible"] === false ? 'none' : 'inline'
                    },
                    filterId: elm.filterId

                };
                switch (elm.filterType) {
                    case 'dateRange':
                        filters.push(<DateRangeFilterComponent {...inputOptions} />);
                        break;
                    case 'date':
                        filters.push(<DateFilterComponent {...inputOptions} />);
                        break;
                    case 'range':
                        inputOptions["valueStart"] = this.props.currentFilters[elm.key + "start"];
                        inputOptions["valueEnd"] = this.props.currentFilters[elm.key + "end"];
                        filters.push(<RangeFilterComponent {...inputOptions} />);
                        break;
                    case 'boolean':
                        filters.push(<BooleanFilterComponent {...inputOptions} />);
                        break;
                    case 'combo':
                        if (elm.valueField == undefined || elm.valueField == "") {
                            inputOptions.valueField = "key";
                        } else {
                            inputOptions.valueField = elm.valueField;
                        }
                        if (elm.displayField == undefined || elm.displayField == "") {
                            inputOptions.displayField = "value";
                        } else {
                            inputOptions.displayField = elm.displayField;
                        }
                        inputOptions.options = elm.filterComboData;

                        inputOptions.mode = elm.comboMode || 'multiple';

                        filters.push(<ComboFilterComponent {...inputOptions} />);
                        break;
                    case 'number':
                        filters.push(<NumberFilterComponent {...inputOptions} />);
                        break;
                    case 'text':
                    default:
                        filters.push(<TextFilterComponent {...inputOptions} />);
                        break;
                }
            }
        }
        return filters;
    };

    clearFilters = () => {
        if(this.props.parent.state.filters.status_com){
            this.props.parent.setState({
                filters: {status_com: this.props.parent.state.filters.status_com}
            });
        }else{
            this.props.parent.setState({
                filters: {}
            });
        }
    };

    search = () => {
        if(this.props.parent.state.pagination&&this.props.parent.state.pagination.current){
            let pagination = this.props.parent.state.pagination;
            pagination.current = 0;
            this.props.parent.setState({pagination: pagination});
        }
        this.props.fetchFunction(this.props.currentFilters)
    };

    render() {
        return (
            <Row type="flex" justify="space-between" className='taggedFilter'>
                <Col xs={12} sm={14} md={16} lg={16} xl={16}>
                    <h3>{T.translate("filters_name")}:</h3>
                    {this.drawFilters(this.props.columns)}
                </Col>
                <Col xs={12} sm={10} md={8} lg={8} xl={8} style={{paddingTop: '21px', textAlign: 'right'}}
                     className='taggedFilterButtons'>
                    <Popover content={this.filterOptions}
                             title={T.translate("filters_available")}
                             placement="bottom"
                             trigger="click">
                        <Button type="primary" style={{marginRight: '5px'}} icon="plus" id="btn_filters_more">
                            {T.translate("filters_more")}
                        </Button>
                    </Popover>
                    <Button type="primary" style={{marginRight: '5px'}}
                            onClick={() => {
                                this.clearFilters()
                            }}
                            icon="close"
                            id="btn_filters_clear">
                        {T.translate("filters_clear")}
                    </Button>
                    {this.props.fetchFunction ? <Button type="primary"
                            onClick={this.search}
                            icon="search"
                            id="btn_filters_search">
                        {T.translate("filters_search")}
                    </Button>:''}
                </Col>
            </Row>
        );
    }
}