import React, { Component } from 'react';
import { Input, Icon } from 'antd';

const InputGroup = Input.Group;

export default class RangeFilterComponent extends Component {


    onChangeStart = (e, key) => {
        this.props.onChange(e, key + "start");
    }

    onChangeEnd = (e, key) => {
        this.props.onChange(e, key + "end");
    }

    render() {
        return (
            <InputGroup compact className='filterComponent filterComponentRange' {...this.props}>
                <Input value={this.props.valueStart} onPressEnter={this.props.onPressEnter} onChange={(e) => this.onChangeStart(e.target.value, this.props.elmkey)} style={{ width: 100, textAlign: 'center' }} addonBefore={this.props.label} placeholder="Minimo" />
                <Input style={{ width: 24, borderLeft: 0, pointerEvents: 'none', backgroundColor: '#fff' }} placeholder="~" disabled />
                <Input value={this.props.valueEnd} onPressEnter={this.props.onPressEnter} onChange={(e) => this.onChangeEnd(e.target.value, this.props.elmkey)} style={{ width: 100, textAlign: 'center', borderLeft: 0 }} placeholder="Máximo" addonAfter={<Icon type="close" onClick={this.props.onCloseClick} />} />
            </InputGroup>
        )
    }
}