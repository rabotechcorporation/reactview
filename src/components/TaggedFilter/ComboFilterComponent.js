import React, { Component } from 'react';
import { Select, Input, Icon } from 'antd';

import lodash from 'lodash';

const InputGroup = Input.Group;
const Option = Select.Option;

export default class ComboFilterComponent extends Component {

    drawOptions = () => {
        let optionsArr = [];
        // optionsArr.push(
        //     <Option key="white" value="">&nbsp;</Option>
        // );
        for (let idx in this.props.options) {
            let elm = this.props.options[idx];
            optionsArr.push(
                <Option key={elm[this.props.valueField] + ""} value={elm[this.props.valueField]}>{elm[this.props.displayField]}</Option>
            )
        }
        return optionsArr;
    }


    onChange = (value, key) => {

            var formattedValue = "";
            var record = {};
            var filter = {};
            if (!Array.isArray(value)) {
                filter[this.props.valueField] = value;
                record = lodash.find(this.props.options, filter);
                formattedValue = record[this.props.displayField] || "";
            } else {
                var formArray = [];
                for (var idx in value) {
                    filter = {};
                    filter[this.props.valueField] = value[idx];
                    record = lodash.find(this.props.options, filter);
                    if(record) {
                        formArray.push(record[this.props.displayField] || "");
                    }
                }
                formattedValue = formArray.join(', ');
            }
            this.props.onChange(value, key, null, formattedValue);

        this.props.onChange(value, key, null, formattedValue);
    }

    render() {

        return (
            <InputGroup compact className='filterComponent' {...this.props}>
                <span style={{
                    lineHeight: '30px',
                    padding: '0 5px',
                    textAlign: 'center',
                    border: '1px solid #d9d9d9',
                    borderRight: 'none',
                    backgroundColor: '#fafafa'
                }}>{this.props.label}</span>
                <Select value={this.props.value} defaultValue={[]} mode={this.props.mode} onPressEnter={this.props.onPressEnter} onChange={(value) => this.onChange(value, this.props.elmkey)} >
                    {this.drawOptions()}
                </Select>
                <Icon type="close" style={{
                    lineHeight: '30px',
                    width: 28,
                    border: '1px solid #d9d9d9',
                    backgroundColor: '#fafafa'
                }} onClick={this.props.onCloseClick} />
            </InputGroup>
        )
    }
}