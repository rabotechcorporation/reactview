import React, { Component } from 'react';
import { Input, DatePicker, Icon } from 'antd';
import moment from 'moment';


const InputGroup = Input.Group;

export default class DateRangeFilterComponent extends Component {



    render() {

        return (
            <InputGroup compact
                className='filterComponent'
                {...this.props}>

                <DatePicker
                    style={{ width: 172 }}
                    format="DD/MM/YYYY"
                    value={moment(this.props.value)}
                    onPressEnter={this.props.onPressEnter}
                    placeholder={this.props.label}
                    onChange={(dates, datestring) => { this.props.onChange(dates, this.props.elmkey, datestring, datestring); }}

                />
                <Icon type="close" style={{
                    float: 'right',
                    lineHeight: '30px',
                    width: 28,
                    border: '1px solid #d9d9d9',
                    backgroundColor: '#fafafa'
                }} onClick={this.props.onCloseClick} />
            </InputGroup>
        )
    }
}