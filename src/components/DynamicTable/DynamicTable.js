import React, { Component } from 'react';
import { Table } from 'antd';
import axios from 'axios';
import async from 'async';
import lodash from 'lodash';

export default class DynamicTable extends Component {

    state = {
        columns: []
    };

    reference;

    constructor(props) {
        super(props);

        //Establecer carga por defecto
        this.state['columns'] = this.props.columns;

        //Establecer relacion con la tabla que utiliza el componente
        this.reference = this.props.parent;
    }

    /**
     * Realiza la carga de las columnas para la tabla actual en funcion del identificador configurado.
     */
    loadColumns = () => {

        let defaultColumns = this.mapDefaultColumns();
        if (defaultColumns.length > 0) {
            let params = {};
            params['center'] = this.props.parent.props.center;
            axios.get('/table/' + this.props.tableIdentifier, {
                withCredentials: true,
                params: params
            })
                .then((data) => {
                    let table = data.data.data;
                    let newColumns = [];
                    let multiColumns = [];
                    //No existe el objeto table, se crea para poder seguir
                    if (!table) table = {};
                    if (!table.configured_columns) table.configured_columns = [];

                    if (table.configured_columns.length === 0) {
                        //Si no hay configuracion se utilizan las columnas por defecto
                        newColumns = this.props.columns;
                    }

                    //Si no hay configuracion no se entra en el bucle
                    async.eachOfSeries(table.configured_columns, (cfgColumn, idx, innerCb) => {

                        //Si es una columna multiple hay que buscar todas las que macheen con ella
                        let columnConfig = lodash.filter(table.available_fields, { 'id': cfgColumn.field_id });
                        if (columnConfig.length > 0 && columnConfig[0].multi === 1) {
                            multiColumns = lodash.filter(defaultColumns, function (o) {

                                if (o.key.indexOf(cfgColumn.field_identifier) !== -1 && o.render) {
                                    //Sobreescribir el renderer para poner el ancho a las columnas mejor
                                    var newFunc = o.render.bind({});
                                    const newRender = (value, record) => {
                                        const currentWidth = o.width && (!isNaN(o.width) ||  o.width.indexOf('%') === -1) ? o.width : undefined;
                                        return newFunc(value, record, currentWidth);
                                    };
                                    o.render = newRender;
                                }

                                return o.key.indexOf(cfgColumn.field_identifier) !== -1;
                            });
                        } else {
                            let defColumn = lodash.find(defaultColumns, { 'dataIndex': cfgColumn.field_identifier });
                            if (defColumn) {
                                // defColumn = defColumn[0];
                                if (cfgColumn.width) {//Configure width
                                    defColumn.width = parseInt(cfgColumn.width);
                                }
                                if (cfgColumn.header_text) {//Configure header text
                                    defColumn.title = cfgColumn.header_text;
                                }
                                if (defColumn.flex) {
                                    defColumn.flex = 1;
                                }
                                if (defColumn.render) {
                                    //Sobreescribir el renderer para poner el ancho a las columnas mejor
                                    var newFunc = defColumn.render.bind({});
                                    const newRender = (value, record) => {
                                        const currentWidth = defColumn.width && (!isNaN(defColumn.width) ||  defColumn.width.indexOf('%') === -1) ? defColumn.width : undefined;
                                        return newFunc(value, record, currentWidth);
                                    };
                                    defColumn.render = newRender;
                                }
                            } else {
                                defColumn = {};
                                let columnConfig = lodash.find(table.available_fields, { 'id': cfgColumn.field_id });
                                defColumn.key = cfgColumn.field_identifier;
                                defColumn.title = cfgColumn.header_text != "" ? cfgColumn.header_text : columnConfig.field_default_text;
                                if (cfgColumn.width && cfgColumn.width > 0) {//Configure width
                                    defColumn.width = parseInt(cfgColumn.width);
                                }
                                if (this.props.dynamicColumnsFilterWildcard) {
                                    defColumn.key = defColumn.key + "##wildcard";
                                }
                                defColumn.dataIndex = cfgColumn.field_identifier;
                                defColumn.filter = true;
                                defColumn.filterOptional = true;
                                defColumn.render = (value, record) => {
                                    const currentWidth = defColumn.width && (!isNaN(defColumn.width) ||  defColumn.width.indexOf('%') === -1) ? defColumn.width : undefined;
                                    return (
                                        <span className="overflow-span" style={{ width: currentWidth ? currentWidth : '100px' }}>
                                            {value}
                                        </span>
                                    );
                                }
                            }
                            newColumns.push(defColumn);
                        }
                        innerCb();
                    }, (err) => {

                        newColumns = newColumns.concat(multiColumns);

                        //Se cambian las columnas de la tabla
                        if (!(this.props.disableAutoSize === true)) {
                            this.setColumnsWidth(newColumns);
                        }
                        //Se cambian las columnas de la tabla
                        this.state.columns = newColumns;

                        let pagination = this.reference.state.pagination ? this.reference.state.pagination : {};
                        if (!pagination.pageSize && table.page_size) { //Si no tiene pageSize se esta cargando la tabla, se pone la por defecto.
                            pagination.pageSize = table.page_size;
                        }

                        this.reference.setState({
                            dynamicColumns: newColumns,

                        });
                        if (this.props.autoLoad) {
                            this.reference.search();
                        }
                    });

                });
        }
    };

    setColumnsWidth = (columns) => {
        let flexCount = 0;
        let fixedColumns = false;
        lodash.forEach(columns, function (column) {
            if (!column.width && !column.fixed) {
                if (!column.flex) {
                    column.flex = 1;
                }
                flexCount += column.flex;
            }

            fixedColumns = !fixedColumns ? column.fixed !== undefined : fixedColumns;
        });
        lodash.forEach(columns, function (column) {
            if (!column.width && !column.fixed) {
                column.width = (column.flex / flexCount) * 100;
                column.width = column.width + "%";
            }
        });

        if (fixedColumns) {
            var scroll = this.props.scroll || {};
            if (scroll) {
                scroll.x = '101%'
            }
            this.setState({ table_scroll: scroll });
        }
    };

    /**
     * Crea un mapa clave valor de las columnas actuales usando la propiedad 'key'
     *
     * @returns {Array}
     */
    mapDefaultColumns = () => {
        let indexedActualColumns = [];
        for (let idx in this.props.columns) {
            let rec = this.props.columns[idx];

            if (rec && rec.key) {
                //indexedActualColumns[rec.key] = rec;
                indexedActualColumns.push(rec);
            }
        }
        return indexedActualColumns;
    };

    /**
     * Evento de carga de la tabla para cargar las columnas
     */
    componentDidMount = () => {
        if (this.props.tableIdentifier) {
            this.loadColumns();
        }
        //window.addEventListener("scroll", this.handleScroll);
    };
    componentDidUpdate = (prevProps, prevState) => {
        if (this.props.columns != prevProps.columns) {
            this.loadColumns();
        }
    };

    render() {
        return (
            <Table {...this.props} columns={this.state.columns} scroll={this.state.table_scroll} />
        );
    }
}