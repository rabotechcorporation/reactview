import React from 'react';
import {Icon} from 'antd';
import lodash from 'lodash';


/**
 *
 * Necesidades de esta clase de utilidades:
 *
 * - La tabla realizara el sort en servidor
 * - Es necesario un metodo handleTableChange que sera el que capture los cambios en los sorts (mirar OrderListTable)
 * - Es necesario que las columnas de la tabla se guarden en el state (mirar OrderListTable)
 * - Es necesario que exista un objeto en el state llamado sortstatus (sortstatus: {})
 * - Es necesario que exista un metodo refreshColumns para que se visualice la ordenacion (mirar OrderListTable)
 * - En cada columna que se quiera ordenar se añadiran las claves classname y onHeaderCell:
 *
 *   className: SortUtils.getIcon(parent.state.sortstatus, 'uop'),
 *      onHeaderCell: (record) => ({
 *         onClick: () => {
 *             SortUtils.sort(parent, record);
 *         }
 *   }),
 *
 *
 */
let SortUtils = {
    sort: (parentComponent, column) => {
        let prevStatus = parentComponent.state.sortstatus[column.dataIndex];
        parentComponent.state.sortstatus = {};
        parentComponent.state.sortstatus[column.dataIndex] = prevStatus == "descend" ? "ascend" : "descend";

        SortUtils.changeIcon(parentComponent.state, column.dataIndex);

        parentComponent.handleTableChange(null, null, {
            field: column.dataIndex,
            order: parentComponent.state.sortstatus[column.dataIndex]
        });
    },
    changeIcon: function(state, dataIndex){
        SortUtils.clearIcon(state.columns);
        var col = lodash.find(state.columns, {dataIndex: dataIndex});
        if(col)
            col.className = SortUtils.getIcon(state.sortstatus, dataIndex);
    },
    getIcon: (status, dataIndex) => {
        if (status) {
            return 'sortable ' + (status && status[dataIndex] ? status[dataIndex] : "");
        }
        return 'sortable';
    },
    clearIcon: (columns) => {
        for(var idx in columns){
            var col = columns[idx];
            col.className = col.className ? col.className.replace('ascend', '').replace('descend','') : col.className;
        }
    }
}
export default  SortUtils;