import React, {Component} from "react";
import {Timeline, Icon} from "antd";
import TimelineCustomItem from './TimelineCustomItem';


export default class CustomTimeline extends Component {


    drawTimelineItems = () => {
        let data = [];
        for (let idx in this.props.data) {
            let elm = this.props.data[idx];
            let icon = undefined;
            if (this.props.selectedItem && this.props.selectedItem.id === elm.id) {
                icon = <Icon type="play-circle-o" style={{fontSize: '16px'}}/>;
            }
            if (elm.action !== 'delete') {
                data.push(<TimelineCustomItem key={idx} color="green" dot={icon}
                                              extraRenderer={this.props.itemExtraRenderer}
                                              onClick={this.props.onItemClick} {...elm} />)
            }
        }
        return data;
    }

    defaultStyle = {width: 300, margin: '10px 0 0 20px', float: 'left'}

    render() {
        return(
            <Timeline pending={this.props.pending}
                      style={this.props.style || this.defaultStyle}>
                {this.drawTimelineItems()}
                {this.props.lastElm}
            </Timeline>
        )
    }


}