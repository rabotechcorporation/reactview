import React, { Component } from "react";
import { Timeline, Popover, Icon } from 'antd';

export default class TimelineCustomItem extends Component {


    loopChildren = () => {
        let data = [];
        for (let idx in this.props.children) {
            let elm = this.props.children[idx]
            if (elm.action !== 'delete') {
                var style = { display: 'inline' }
                if (elm.color) {
                    style.color = elm.color;
                }

                var extra = null;
                if (elm.extra) {
                    extra = (
                        <Popover
                            content={this.props.extraRenderer(elm)}
                            title={elm.extra.title}
                            trigger="click"
                        >
                            <Icon
                                type={"info-circle"}
                                style={{
                                    fontSize: '16px',
                                    marginRight: "10px",
                                    cursor: 'pointer'
                                }}
                            />
                        </Popover>
                    )
                }

                data.push((
                    <div key={idx} style={{ margin: '8px 0px' }}>
                        <p style={style} dangerouslySetInnerHTML={{ __html: elm.text }}></p>
                        {extra}
                    </div>
                ));
            }
        }
        return data;
    }

    onClick = () => {
        this.props.onClick(this.props.id);
    }

    render() {

        var props = { ...this.props };
        delete props.extraRenderer;
        return (
            <Timeline.Item  {...props} onClick={this.onClick} style={{ cursor: 'pointer' }}
                className="timelineCustomItem">
                <p ><b>{this.props.text}</b></p>
                {this.loopChildren()}
            </Timeline.Item>
        )
    }
}