import {Checkbox,Form,  Input, Button, Select} from 'antd';
import React, {Component} from "react";
import axios from "axios/index";
import async from 'async';
import T from "i18n-react/dist/i18n-react";


/**
 * Generador dinámico de formularios en base a props pasadas por la clase padre.
 *
 * Propiedades: lo suyo es pasarle arrays completos de todos los elementos para evitar problemas, pero en algunos
 * casos no es necesario.
 *
 * fields: identificador del campo, se usará a la hora de hacer el submit en la base de datos (vamos, que coincida
 * con el atributo del objeto a almacenar).
 * fieldNames: nombre que se mostrará en el placeHolder de la vista.
 * types: tipos de elementos. Por ahora admite text, password y combo. Default: text.
 * stores: array de stores necesarios para poblar los combos. Cada elemento estará compuesto del field del elemento
 *         como identificador y la url de carga. Ejemplo: {id: 'nombreDelField', url: '/ruta/servidor'}
 * post: ruta con la que hacer el guardado.
 * required: si el campo es requerido para la validación del formulario.
 * handleAdd: método del padre para tratar los estados (success) del guardado de los datos.
 * clearButton: indica si se añade el botón de limpiar campos. No es necesario, para añadirlo: clearButton={true}
 * layout: indica el layoute del formulario (horizontal, vertical, inline). Defecto: horizontal.
 *
 * Ejemplo de formulario completo para añadir un usuario con los campos nombre, contraseña y perfil. Contiene
 * un campo tipo text, un campo tipo password y un combo con su store, los tres requeridos. El formulario mostrará
 * un botón de limpiar campos:
 *
 * <GenericAddForm fields={['username', 'newPassword', 'profileids']}
 * fieldNames={['Nombre', 'Contraseña', 'Perfiles']}
 * types={['text', 'password', 'combo']}
 * stores={[{id: 'profileids', url: '/profile/list'}]}
 * post={'/user'}
 * required={[true, true, true]}
 * handleAdd={this.handleAdd}
 * clearButton={true}
 * layout={'vertical'}/>
 *
 */
class AddForm extends Component {

    constructor(props){
        super(props);

        this.state  = {
            fields: this.props.fields,
            fieldNames: this.props.fieldNames,
            types: this.props.types,
            stores: this.props.stores,
            clear: this.props.clearButton,
            required: this.props.required
        };
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(e){

    }

    hasErrors = (fieldsError) => {
        return Object.keys(fieldsError).some(field => fieldsError[field]);
    };

    componentDidMount() {
        // To disabled submit button at the beginning.
        this.props.form.validateFields();
        if(this.state.stores){
            this.loadStores(this.state.stores);
        }
    };

    // componentWillUpdate(prevProps, prevState){
    //     if(!this.props.closeForm){
    //         this.clear();
    //     }
    // }

    /**
     * Valida los campos y manda el objeto al servidor para su guardado en la BD y llama al método padre para que maneje
     * el éxito o fracaso de la operación.
     *
     * @param e event
     */
    handleSubmit = (e) => {
        const self = this;
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
            if (!err) {
                axios.post(this.props.post, values, {
                    withCredentials: true
                }).then((data) => {
                    self.props.handleAdd(data.data);

                    this.clear();
                })
            }
        });
    };

    /**
     * Carga las stores necesarias para el formulario y las setea en el state.
     * @param stores
     */
    loadStores = (stores) => {
        let self = this;
        async.forEachOfSeries(
            stores,
            function (store, i, callback) {
                if(store.url && store.url != ""){
                    axios.get(store.url, {
                        withCredentials: true
                    }).then((data) => {
                        self.state.stores[i].store = data.data.data;
                        callback();
                    });
                }else{
                    callback();
                }
            },
            function (err) {
                if(err) console.log(err);
                self.forceUpdate();
            }
        );
    };

    /**
     * Carga las opciones de los combos con las stores anteriormente cargadas.
     * @param field el identificador del combo.
     * @returns {Array} Options del combo.
     */
    loadOptions = (field) => {
        let options = [];
        for(let i = 0;i<this.state.stores.length;i++){
            if(this.state.stores[i].id==field&&this.state.stores[i].store){
                for(let j = 0;j<this.state.stores[i].store.length;j++){
                    options.push(
                        <Select.Option key={this.state.stores[i].store[j].id}
                                       value={this.state.stores[i].store[j].id.toString()}>{this.state.stores[i].store[j].name}</Select.Option>
                    )
                }
            }
        }
        return options;
    };

    /**
     * Genera un componente del formulario en función de los parámetros suministrados
     *
     * @param field id del campo.
     * @param fieldName nombre del placeHolder.
     * @param type tipo de campo.
     * @param required si es requerido
     * @param getFieldDecorator decorator del campo.
     * @returns {*} el campo para su renderizado.
     */
    returnComp = (field,fieldName,type,required,getFieldDecorator) => {
        switch(type){
            case 'combo':
                return (
                    getFieldDecorator(field, {rules: [{required: required, message: T.translate("common_field_mandatory")}]})
                    (<Select getPopupContainer={triggerNode => triggerNode.parentNode}
                            placeholder={fieldName != undefined ? fieldName : ''} style={{minWidth: 150}}>
                        {this.loadOptions(field)}
                    </Select>)
                );
            case 'check':
                return (
                    getFieldDecorator(field, {
                        rules: [{required: required, message: T.translate("common_field_mandatory")}],
                        valuePropName: 'checked',
                        initialValue: false,
                    })
                    (<Checkbox>{fieldName != undefined ? fieldName : ''}</Checkbox>)
                );
            default:
                return (
                    getFieldDecorator(field, {rules: [{required: required, message: T.translate("common_field_mandatory")}]})
                    (<Input id="" type={type == 'password' ? 'password' : undefined}
                            placeholder={fieldName != undefined ? fieldName : ''}/>)

                );
        }
    };

    /**
     * Genera el elemento padre (ítem de un formulario) de los campos del formulario.
     *
     * @param fields ids de los campos del formulario.
     * @param fieldNames nombres de los campos.
     * @param types tipos de los campos.
     * @param required si son requeridos.
     * @param fieldError control de errores en los campos.
     * @param getFieldDecorator decorator de los campos.
     * @returns {Array} devuelve los Form.Items.
     */
    dynamicFormItem = (fields,fieldNames,types,required,fieldError,getFieldDecorator) => {
        let components = [];
        for(let i = 0;i<fields.length;i++){
            components.push(
                <Form.Item
                    key={fields[i]+i}
                    validateStatus={fieldError[i] ? 'error' : ''}
                    help={fieldError[i] || ''}
                >
                    {this.returnComp(fields[i],fieldNames[i],types[i],required[i],getFieldDecorator)}
                </Form.Item>
            );
        }
        return components;
    };

    /**
     * Limpia los campos y hace un validateFields para desactivar el botón Aceptar.
     */
    clear = () => {
        this.props.form.resetFields();
        this.props.form.validateFields();
    };

    /**
     * Render.
     * @returns {*} devuelve el formulario renderizado.
     */
    render() {
        const { getFieldDecorator, getFieldsError, getFieldError, isFieldTouched } = this.props.form;

        const {fields, fieldNames, types, required, clear} = this.state;

        let fieldError = {};

        for(let i = 0;i<fields.length;i++){
            fieldError[i] = isFieldTouched(fields[i]) && getFieldError(fields[i]);
        }

        return (
            <Form layout={this.props.layout?this.props.layout:"horizontal"} id="generic-form" onSubmit={this.handleSubmit}
                  style={this.props.layout && this.props.layout == 'vertical' ? {marginTop: 30}:{}}>
                {this.dynamicFormItem(fields,fieldNames,types,required,fieldError,getFieldDecorator)}
                <br />
                <Form.Item style={{float:'right'}}>
                    <Button
                        type="primary"
                        htmlType="submit"
                        style={{marginRight: 10}}
                        disabled={this.hasErrors(getFieldsError())}
                        id="btn_add_ok"
                    >
                        {T.translate("common_button_save")}
                    </Button>
                    <Button
                        type="primary"
                        htmlType="submit"
                        style={{marginRight: 10}}
                        id="btn_add_cancel"
                        onClick={this.props.handleCancel}
                    >
                        Cancelar
                    </Button>
                    {clear ? <Button onClick={this.clear}>{T.translate('config-button-clear')}</Button>:''}
                </Form.Item>
            </Form>
        );
    }
}

const GenericAddForm = Form.create()(AddForm);

export default GenericAddForm;