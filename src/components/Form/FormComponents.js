/**
 * Created by Trabajo on 14/12/17.
 */
import React, { Component } from 'react';
import { Input, Button, Popconfirm, Modal } from 'antd';
import T from "i18n-react/dist/i18n-react";

import ComboBox from '../ComboBox'

import './FormComponents.css';

const { TextArea } = Input;


export class FormTextField extends Component {

    state = {
        record: '',
    }

    onChange = (value) => {
        let record = this.props.record;
        if (this.props.dataindex) {
            record[this.props.dataindex] = value.target.value;
        } else {
            record = value.target.value;
        }
        this.setState({ record: record });

        const onChange = this.props.onChange;
        if (onChange) {
            onChange(Object.assign({}, this.state, value));
        }
    }

    render() {
        var self = this;

        return (

            <Input {...self.props} onChange={self.onChange} />
        )
    }
}

export class FormTextArea extends Component {

    state = {
        record: ""
    }



    componentDidUpdate(prevProps, prevState) {
        if (prevProps.record !== this.props.record) {
            this.setState({ record: this.props.record });
        }

    }

    onChange = (value) => {
        let record = this.props.record;
        record[this.props.dataindex] = value.target.value;
        this.setState({ record: record });

        const onChange = this.props.onChange;
        if (onChange) {
            onChange(Object.assign({}, this.state, value));
        }
    }

    render() {
        return (

            <TextArea rows={this.props.rows}
                {...this.props}
                onChange={this.onChange}
            />

        )
    }
}

export class FormComboBox extends ComboBox {

    state = {
        record: ""
    }

    constructor(props) {
        super(props);
        this.valuefield = this.props.valuefield ? this.props.valuefield : ""
        this.displayfield = this.props.displayfield ? this.props.displayfield : ""
    }

    render() {
        return (

            <ComboBox {...this.props}
                prefix={this.props.prefix}
                dropdownMatchSelectWidth={false}
                style={{ ...this.props.style, minWidth: '150px' }}
                options={this.props.options}
            />

        )
    }
}

export class FormActionButtons extends Component {
    state = {};


    saveForm = () => {
    }
    cancelForm = () => {

    }

    check = () => {
        var self = this;
        this.props.saveFields(function (err) {
            if (err) console.log(err)
        })
    };

    modal = () => {
        Modal.confirm({
            title: T.translate("integrationdetail_extra_warning_title"),
            content: T.translate("integrationdetail_extra_warning"),
            okText: T.translate("common_yes"),
            cancelText: T.translate("common_no"),
            onOk: this.props.saveForm,
            iconType: 'exclamation-circle'
        })
    };
    /*
     <Button key="clear"
     type="primary"
     size="large"
     className="actionButtons"
     onClick={this.props.clearForm}>
     {T.translate('common_button_clean')}
     </Button>
     */
    render() {
        return (
            <div style={{ float: 'right', paddingLeft: '10px' }}>
                {this.props.record.id ? <Popconfirm title={T.translate("integrationdetail_default_config_warning")}
                    onConfirm={this.props.defaultConf}>
                    <Button key="submit"
                        type="dashed"
                        size="large"
                        className="actionButtons">
                        {T.translate("integrationdetail_button_default_config")}
                    </Button>
                </Popconfirm> : ''}
                <Button key="submit"
                    type="primary"
                    size="large"
                    className="actionButtons"
                    onMouseEnter={this.check}
                    onClick={this.props.warning ? this.modal : this.props.saveForm}
                >
                    {T.translate('common_button_accept')}
                </Button>
                <Popconfirm title={T.translate("common_question")} onConfirm={() => this.props.cancelForm()}>
                    <Button key="cancel"
                        size="large"
                        className="actionButtons">
                        {T.translate('common_button_cancel')}
                    </Button>
                </Popconfirm>
            </div>
        );
    }
}

