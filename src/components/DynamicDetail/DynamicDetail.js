import React, {Component} from 'react';
import T from "i18n-react/dist/i18n-react";


export default class DynamicDetail extends Component {

    drawElement = (template, key, data) => {
        let label = template;
        let value = data[key];
        let style = {marginRight: '15px'};
        if (key.indexOf('.') !== -1) {
            let subkey = key.split('.');
            if (data[subkey[0]]) {
                value = data[subkey[0]][subkey[1]];
            }
        }

        if (typeof template === 'object') {
            label = template.label
            style = Object.assign(style, template.style);
            if (template.renderer) {
                value = template.renderer(value);
            }
        }

        if (label) {
            let translation = T.translate(label);
            label = translation.indexOf('undefined') !== -1 ? label : translation;
        }

        return (
            <div key={key} style={style}>{label} <span style={{fontWeight: 'bold'}}>{value} </span></div>
        )
    }

    applyPattern = (pattern, dataObj) => {
        let data = [];
        for (let idx in pattern) {
            let subdata = [];
            if (typeof pattern[idx] === 'object' && idx.indexOf('container') !== -1) {
                subdata = <div key={idx} style={{marginBottom: '15px'}}>{this.applyPattern(pattern[idx], dataObj)}</div>
            } else {
                subdata.push(this.drawElement(pattern[idx], idx, dataObj));
            }

            data.push(subdata);
        }

        return data;
    }

    drawTemplate = (data) => {
        if (data) {
            return this.applyPattern(this.props.pattern, data);
        }
        return;
    }

    render() {
        return (
            <span>{this.drawTemplate(this.props.data)}</span>
        )

    }
}