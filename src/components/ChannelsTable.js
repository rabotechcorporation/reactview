import React, { Component, Router, Route, Link } from 'react';
import T from "i18n-react/dist/i18n-react";

import DynamicTable from './DynamicTable/DynamicTable';

import {withRouter} from "react-router-dom";
import axios from 'axios';


class ChannelsTable extends Component {

    state = {
        data: [],
        pagination: {},
        loading: false,
        render: false,
        leftClickVisible: false,
        centerList: [],
        editionMode: false,
        editionRecord: {}
    };

    tableProps = {
        bordered: true,
        loading: false,
        pagination: true,
        size: 'small',
        rowSelection: {},
        rowKey: 'id',
    };

    componentDidMount() {
        this.search();
    }

    componentDidUpdate(prevProps, prevState) {
        if (prevProps.selectedChannels !== this.props.selectedChannels) {
            this.setState({selectedRowKeys: this.props.selectedChannels});
        }
    }

    search = (params = {}) => {
        this.setState({
            loading: true,
            data: []
        });
        const pagination = {...this.state.pagination};
        if (!params.start && !params.limit) {
            params.limit = pagination.pageSize ? pagination.pageSize : 10;
            params.start = (pagination.current ? pagination.current : 1 - 1) * (pagination.pageSize ? pagination.pageSize : 10);
        }


        if (this.props.center) {
            params.center = this.props.center;
        }

        axios.get('/channel/list', {
            params: params,
            withCredentials: true
        }).then((data) => {
            // Read total count from server
            pagination.total = data.data.total;
            pagination.showSizeChanger = true;
            this.setState({
                loading: false,
                data: data.data.data,
                pagination,
            });
        });
    }

    getAll = (self)=>{
        return self.state.data;
    };

    getCurrentPage = (self)=>{
        return [];
    };

    getSelection = (self)=>{
        return [];
    };

    onChangeSelection = (selectedRowKeys/*value1, value2, value3, value4, value5, value6, value7, value8, value9*/)=>{
        // console.log([value1, value2, value3, value4, value5, value6, value7, value8, value9]);
        this.setState({selectedRowKeys});
    };


    getStructure = () => {
        return [
            {
                title: T.translate('integrationslist_column_name'),
                specialLabel: T.translate('integrationslist_filter_name'),
                dataIndex: 'id',
                key: 'channel.id'
            },
            {
                title: T.translate('integrationslist_column_name'),
                specialLabel: T.translate('integrationslist_filter_name'),
                dataIndex: 'name',
                key: 'channel.name'
            }
        ];
    }

    render() {
        const Home = () => (
            <div>
                <h2>Home</h2>
            </div>
        )



        const rowSelection = {
            onChange: this.onChangeSelection,
            selectedRowKeys: this.state.selectedRowKeys
        };
        return (
            <div>
                <div>
                    <DynamicTable
                        columns={this.getStructure()}
                        {...this.tableProps}

                        rowSelection={rowSelection}
                        dataSource={this.state.data}
                        pagination={this.state.pagination}
                        loading={this.state.loading}
                    />
                </div>
            </div>
        )
    }
};

export default withRouter(ChannelsTable);