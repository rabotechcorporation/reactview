import React, { Component } from 'react';
import { Select } from 'antd';

const Option = Select.Option;

export default class ComboBox extends Component {

    componentDidUpdate(prevProps, prevState) {
        if(prevProps.record !== this.props.record){
            //this.setState({record: this.props.record});
        }

    }

    drawOptions = () => {
        let optionsArr = [];
        const {options, showEmptyValue} = this.props;
        let {valuefield, displayfield} = this.props;

        if(!valuefield){
            valuefield = "key";
        }
        if(!displayfield){
            displayfield = "value";
        }

        if(showEmptyValue !== undefined && showEmptyValue === true) {
            optionsArr.push(
                <Option key="white" value="">&nbsp;</Option>
            );
        }
        for (let idx in options) {
            let elm = options[idx];
            optionsArr.push(
                <Option key={elm[valuefield]+""} value={elm[valuefield]+""} disabled={elm.disabled}>{elm[displayfield]}</Option>
            )
        }
        return optionsArr;
    }

    render() {
        return (

            <Select {...this.props} >
                {this.drawOptions()}
            </Select>

        )
    }
}