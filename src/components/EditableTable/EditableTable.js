import React, {Component} from 'react';
import {Checkbox, Icon, Input, InputNumber, Popconfirm, Select, Switch, Table, Tooltip} from 'antd';

import './EditableTable.css'
import axios from "axios/index";
import T from "i18n-react/dist/i18n-react";

import lodash from 'lodash'

const Option = Select.Option;

/**
 * Clase para generar tablas de forma dinámica en función de las columnas y la posible edición de registros fila a fila.
 *
 * Props que recibe:
 *
 * dataSource: el dataSource para cargar los datos en la Table.
 * pagination: número de páginas del paginador.
 * loading: estado de carga de la pantalla.
 * columns: columnas con sus atributos, los cuales se definen en la clase padre:
    title: título de la columna.
    dataIndex: dataIndex de la columna.
    key: clave elemento HTML.
    type: tipo de la columna. Valores actuales: password, number, multicombo. Default: Input texto.
    values: valores de la columna, en caso de ser de tipo multicombo,
    store: en caso de ser multicombo, ruta de la consula en servidor para obtener todos los elementos del combo a cargar.
    filter: si tiene elemento de filtrado.
 *
 *
 * saveRecord: método de guardado de la clase padre. Parámetros: record.
 * deleteRecord: método de borrado de la clase padre. Parámetros: id del registro.
 *
 */
class EditableTable extends Component {

    state = {
        pagination: {},
        loading: false,
        data: []
    };

    tableProps = {
        bordered: true,
        tableIdentifier: 'tabl',
        loading: false,
        pagination: true,
        size: 'small',
        rowKey: "id"
    };

    //Id de la fila anterior, en caso de darle a dos filas ditintas a editar, para cancelar la anterior.
    currentId = '';

    editing = false;

    columns = [];

    constructor(props) {
        super(props);
        var self = this;
        if(this.props.preActionColumns){
            for(let idx in this.props.preActionColumns){
                this.columns.push(this.props.preActionColumns[idx])
            }
        }

        this.columns.length == 0 ? this.columns = this.props.columns : this.columns.push(...this.props.columns);
        if(this.props.pagination){
            this.tableProps.pagination = this.props.pagination;
        }

        //Renderizado de las columnas pasadas por la clase padre.
        for(let i = 0;i<this.columns.length;i++){
            if(!self.columns[i].action){
                if(!self.columns[i].render){
                    self.columns[i].render = (text, record) => self.renderColumns(text, record, self.columns[i]);
                }
                //Carga las stores, de ser necesario.
                if(self.columns[i].store && !Array.isArray(self.columns[i].store)){
                    if(self.columns[i].loadStore == undefined || self.columns[i].loadStore == true){
                        axios.get(self.columns[i].store, {
                            withCredentials: true
                        }).then((data) => {
                            self.columns[i].store = data.data.data;
                        });
                    }
                }
            }
        }
        if(this.props.actionColumns){
            for(let idx in this.props.actionColumns){
                this.columns.push(this.props.actionColumns[idx])
            }
        }
        //Añade las columnas de acciones.
        if(this.props.saveRecord){
            this.columns.push(
                {
                    dataIndex: 'edit',
                    width: 60,
                    render: (text, record) => {
                        const { editable } = record;
                        if(this.props.tableID=='UserConfig'&&record.id==1||this.props.tableID=='ProfileConfig'&&record.id==1){
                            return('');
                        }else{
                            return (
                                <div className="table-action-column">
                                    {
                                        editable ?
                                            <span>
                                        <Tooltip title={T.translate("common_button_save")} placement="bottom">
                                            <Icon
                                                type="check"
                                                className="editable-table-icon-check"
                                                onClick={() => this.save(record.id)}
                                                style={{marginRight: 3}}
                                            />
                                        </Tooltip>
                                        <Tooltip title={T.translate("common_button_cancel")} placement="bottom">
                                            <Icon
                                                type="close"
                                                className="editable-table-icon"
                                                onClick={() => this.cancel(record.id)}
                                            />
                                        </Tooltip>
                                    </span>
                                            :
                                            <span>
                                        <Tooltip title={T.translate("common_edit")} placement="bottom">
                                            <Icon
                                                type="edit"
                                                className="editable-table-icon"
                                                onClick={() => this.edit(record.id)}
                                                style={{marginRight: 3}}
                                            />
                                        </Tooltip>
                                    </span>
                                    }
                                </div>
                            );
                        }
                    },
                }
            );
        }

        if(this.props.deleteRecord){
            this.columns.push({
                dataIndex: 'delete',
                width: 40,
                render: (text, record) => {
                    if(this.props.tableID=='UserConfig'&&record.id==1||this.props.tableID=='ProfileConfig'&&record.id==1){
                        return('');
                    }
                    return (
                        <div className="table-action-column">
                            <Tooltip title={T.translate("common_button_delete")} placement="bottom">
                                <Popconfirm title={T.translate("common_question")}
                                            onConfirm={() => this.delete(record.id)}>
                                    <Icon
                                        type="delete"
                                        className="editable-table-icon"
                                    />
                                </Popconfirm>
                            </Tooltip>
                        </div>
                    );
                }
            });
        }
        //this.cacheData = props.dataSource.map(item => ({ ...item }));
    }

    /**
     * Esto creo que ya no vale para nada.
     */
    componentWillMount(){
        this.setState({data: this.props.dataSource});
    }

    /**
     * Renderizado de las columnas. Utiliza los métodos siguientes en función del tipo de elemento a renderizar.
     * @param text
     * @param record
     * @param column
     * @returns {*}
     */
    renderColumns(text, record, column) {
        if(column.displayField && column.store){

            let element = lodash.find(column.store, {'id': text+""});
            if(!element){
                element = lodash.find(column.store, {'id': text})
            }
            if(element){
                text = element[column.displayField]
            }
        }

        return (
            <div className="editable-table">
                {
                    record.editable ?
                        <div className="editable-table-input-wrapper">
                            {this.setEditType(column.type, text, column.store, record[column.values], record, column)}
                        </div>
                        :
                        <div className="editable-table-text-wrapper">
                            {this.setShowType(column.type, text, column.emptyValue ? column.emptyValue : '')}
                        </div>
                }
            </div>
        )
    }

    /**
     * Devuelve un tipo de elemento de edición en función del type suministrado.
     *
     * @param type tipo de elemento.
     * @param value valor.
     * @param store en caso de ser multicombo, todos los elementos posibles a cargar en el combo.
     * @param current en caso de ser multicombo, los elementos actualmente activos en ese registro.
     * @param record registro.
     * @param column columna.
     * @returns {*} el elemento de edición correspondiente.
     */
    setEditType = (type, value, store, current, record, column) => {
        if(!column.notEditable){
            switch (type) {
                case 'password':
                    return (
                        <div>
                            <Input
                                type="password"
                                value={value}
                                onChange={e => this.handleChange(e.target.value, record.id, column)}
                                onPressEnter={this.check}
                                onKeyDown={this.handleKeyPress}
                                ref={input => {this.passwordInput = input}}
                                onFocus={this.clearPassword}
                            />
                        </div>
                    );
                case 'number':
                    let element = '';
                    column.min != undefined ?
                        element =
                            <div>
                                <InputNumber
                                    value={value}
                                    onChange={e => this.handleChange(e, record.id, column)}
                                    onPressEnter={this.check}
                                    onKeyDown={this.handleKeyPress}
                                    min={column.min}
                                />
                            </div>
                        :
                        element =
                            <div>
                                <InputNumber
                                    value={value}
                                    onChange={e => this.handleChange(e, record.id, column)}
                                    onPressEnter={this.check}
                                    onKeyDown={this.handleKeyPress}
                                />
                            </div>;
                    return (
                        element
                    );
                case 'multiCombo':
                    let options = [];
                    for (let i = 0; i < store.length; i++) {
                        options.push(<Option key={store[i].id} value={store[i].id.toString()}>{store[i].name}</Option>)
                    }
                    let currentValues = [];
                    for (let i = 0; i < current.split(', ').length; i++) {
                        currentValues.push({key: current.split(', ')[i]})
                    }
                    return (
                        <div>
                            <Select
                                labelInValue
                                defaultValue={currentValues[0].key != "" ? currentValues : []}
                                style={{width: '100%'}}
                                mode="multiple"
                                onChange={e => this.handleChange(e, record.id, column)}
                            >
                                {options}
                            </Select>
                        </div>);
                case 'combo':
                    options = [];
                    for (let i = 0; i < store.length; i++) {
                        options.push(<Option key={store[i].id} value={store[i].id.toString()}>{store[i].name}</Option>)
                    }
                    currentValues = [];
                    for (let i = 0; i < current.split(', ').length; i++) {
                        currentValues.push({key: current.split(', ')[i]})
                    }
                    return (
                        <div>
                            <Select
                                labelInValue
                                defaultValue={currentValues[0].key != "" ? currentValues : []}
                                style={{width: '100%'}}
                                onChange={e => this.handleChange(e, record.id, column)}
                            >
                                {options}
                            </Select>
                        </div>);
                case 'check':
                    return (
                        <div>
                            <Checkbox checked={value == 1 ? true : false} onChange={e => this.handleChange(e, record.id, column)}></Checkbox>
                        </div>
                    );
                default:
                    return (
                        <div>
                            <Input
                                value={value}
                                onChange={e => this.handleChange(e.target.value, record.id, column)}
                                onPressEnter={this.check}
                                onKeyDown={this.handleKeyPress}
                            />
                        </div>
                    );
            }
        }else{
            return (
                <span style={{padding: '5px 24px 5px 5px'}}>{value}</span>
            );
        }
    };

    /**
     * Devuelve un valor a mostrar en función del type suministrado.
     *
     * @param type tipo de elemento.
     * @param value valor.
     * @param emptyValue bastante descriptivo. No vale de nada, pero por ahí anda de un copy-paste.
     * @returns {*} el elemento a renderizar.
     */
    setShowType = (type, value, emptyValue) => {
        switch (type) {
            case 'password':
                return (
                    <div>
                        {'••••••••'}
                    </div>
                );
            case 'switch':
                return (<Switch checked={value == 1} /> );
            case 'multiCombo':
                return (
                    <div>
                        {Array.isArray(value) ? value.join(', ') : value}&nbsp;
                    </div>
                );
            case 'check':
                return(
                    <div>
                        {value == 1 ? 'Si' : 'No'}
                    </div>
                );
            case 'combo':
            default:
                return (
                    <div>
                        {value || emptyValue || ' '}
                    </div>
                );
        }
    };

    /**
     * Cancela la edición con la tecla Esc. No funciona en los multicombos.
     *
     * @param e event
     */
    handleKeyPress = (e) => {
        if (e.key == 'Escape') {
            this.cancel(this.currentId);
        }
    };

    /**
     * Método encargado de manejar los cambios de los elementos a la hora de editar.
     *
     * @param value valor editado.
     * @param id id registro.
     * @param column columna
     */
    handleChange(value, id, column) {
        const newData = [...this.state.data];
        const target = newData.filter(item => id === item.id)[0];
        if (target) {
            if(column.values){
                if(column.type == "combo"){
                    target[column.values] = value.key
                }else{
                    target[column.values] = value.map(function(elem){
                        return elem.key;
                    }).join(', ');
                }
                
            }else{
                if(column.type === "check"){
                    target[column.dataIndex] = value.target.checked === true ? 1 : 0
                }else{
                    target[column.dataIndex] = value;
                    if(column.dataIndex==='password'){
                        target.newPassword = value;
                    }
                }
                
            }
            this.setState({ data: newData });
        }
    }

    /**
     * Limpia la contraseña cuando entra el foco en su input.
     */
    clearPassword = () => {
        this.passwordInput.input.value = '';
    };

    /**
     * Método que habilita la edición.
     *
     * @param id id del registro.
     */
    edit(id) {
        //Para cancelar edición si ya se está editando, sin recargar la página.
        if(this.editing){
            const newData = [...this.props.dataSource];
            const target = newData.filter(item => this.currentId === item.id)[0];
            if (target) {
                Object.assign(target, this.cacheData.filter(item => this.currentId === item.id)[0]);
                delete target.editable;
                this.setState({ data: newData });
            }
            delete this.cacheData;
        }
        if(!this.cacheData){
            this.cacheData = this.props.dataSource.map(item => ({ ...item }))
        }
        const newData = [...this.props.dataSource];
        const target = newData.filter(item => id === item.id)[0];
        if (target) {
            target.editable = true;
            // if(target.password){
            //     target.password = '';
            // }
            this.setState({ data: newData });
            this.currentId = id;
            this.editing = true;
        }
    }

    /**
     * Método de guardado. Si to_do va bien, llama al método de guardado de la clase padre y le manda el registro.
     *
     * @param id id del registro.
     */
    save(id) {
        const newData = [...this.state.data];
        const target = newData.filter(item => id === item.id)[0];
        if (target) {
            delete target.editable;
            this.props.saveRecord(target);
        }
        this.editing = false;
    }

    /**
     *  Método de cancelación de edición.
     * @param id id del registro.
     */
    cancel(id) {
        const newData = [...this.props.dataSource];
        const target = newData.filter(item => id === item.id)[0];
        if (target) {
            Object.assign(target, this.cacheData.filter(item => id === item.id)[0]);
            delete target.editable;
            this.setState({ data: newData });
        }
        delete this.cacheData;
        this.editing = false;
        if(this.props.cancelEdit){
            this.props.cancelEdit(this.props.currentFilters);
        }
    }

    /**
     *  Método de eliminación. Llama al método de eliminación del padre y le manda el id. Podría llamarse directamente
     *  desde el botón, pero así queda más ordenado.
     *
     * @param id id del registro.
     */
    delete(id){
        this.props.deleteRecord(id);
    }

    /**
     * Render de la tabla.
     *
     * @returns {*}
     */
    render() {
        return <Table {...this.tableProps}
                      dataSource={this.props.dataSource}
                      columns={this.columns}
                      pagination={this.props.pagination}
                      loading={this.props.loading}
                      bordered={this.props.bordered != undefined ? this.props.bordered : true}
                />;
    }
}

export default EditableTable;