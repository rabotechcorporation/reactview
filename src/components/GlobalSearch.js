/**
 * Created by iago.sardina on 28/11/2017.
 */
import React, {Component} from 'react';
import {Input, List, Popover} from 'antd';
import {Link} from 'react-router-dom';

import lodash from 'lodash';
import moment from 'moment';
import axios from 'axios';
import './GlobalSearch.css';
//import OrderListCommon from '../views/OrderList/OrderListCommon';

const Search = Input.Search;

//const orderCommon = new OrderListCommon();
export default class GlobalSearch extends Component {

    state = {
        loading: false,
        viewDataVisible: false,
        data: []
    }


    performSearch = (value) => {
        this.setState({
            loading: true,
        });

        if(!this.props.currentCenter){
            return;
        }

        let centerIds = [];
        if (this.props.currentCenter === "-") {
            centerIds = lodash.map(this.props.centerList, 'id');
            let index = centerIds.indexOf("-");
            centerIds.splice(index, 1);
        } else {
            centerIds = [this.props.currentCenter];
        }
        let centerFilter = "+(center:"+centerIds.join(' OR center:')+")";

        value = centerFilter+' -_type:(message) +' + value

        axios.get('/status_control/search', {
            params: {
                'query': value
            },
            withCredentials: true
        }).then((data) => {
            this.setState({
                loading: false,
                viewDataVisible: true,
                data: data.data.data
            })
        });
    }


    drawOrder = (item) => {
        let order = item._source;
        return (
            <List.Item >
                <List.Item.Meta
                    //avatar={orderCommon.getUseAvatar(order.use)}
                    title="Petición"
                    description={
                        <div>
                            <Link to={"/order/" + order.id } key={order.id}>{order.orderIdOrigin}</Link> | {moment(order.date).format("DD/MM/YYYY HH:mm:ss")}
                        </div>
                    }
                />
                <div></div>
            </List.Item>
        )

    }

    drawMessage = (item) => {
        let message = item._source;
        let msgType = "";
        let msg = message.destination[0] ? message.destination[0].transformedMessage.transformedMessageJson : undefined;
        if (!msg) {
            msg = message.source.sourceMessage.sourceMessageJson;
        }
        if (msg) {
            msgType = Object.keys(msg) ? Object.keys(msg)[0] : "";
        }
        return (<List.Item actions={[<a>view</a>]}>
            <List.Item.Meta

                title='Mensaje'
                description={item._id + ' | ' + msgType}
            />
            <div></div>
        </List.Item>);
    }

    drawDefault = (item) => {
        // let message = item._source;
        return (<List.Item actions={[<a>view</a>]}>
            <List.Item.Meta

                title={item._type}
                description={item._id}
            />
            <div></div>
        </List.Item>);
    }

    searchOverContent = () => {
        return (
            <List
                className="resultList"
                loading={this.state.loading}
                itemLayout="horizontal"
                dataSource={this.state.data}
                style={{width: 350, maxHeight: 400}}
                renderItem={item => {
                    switch (item._type) {
                        case 'order':
                            return this.drawOrder(item);
                        case 'message':
                            return this.drawMessage(item);
                        default:
                            return this.drawDefault(item);
                    }
                }}
            />
        )
    }

    handleVisibleChange = (visible) => {
        this.setState({viewDataVisible: visible});
    }

    render() {
        return (
            <div >
                <Popover
                    content={this.searchOverContent()}
                    title="Búsqueda Global"
                    placement="bottom"
                    visible={this.state.viewDataVisible}
                    trigger="click"
                    onVisibleChange={this.handleVisibleChange}
                    className="searchOverContent">

                    <Search
                        placeholder="Búsqueda Global"
                        onSearch={this.performSearch}
                        enterButton
                    />

                </Popover>

            </div>
        )
    }
}