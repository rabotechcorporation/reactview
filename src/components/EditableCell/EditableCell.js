import React, {Component} from 'react';
import {Input, InputNumber, Icon, Switch, Select, Tooltip} from 'antd';
import T from "i18n-react/dist/i18n-react";

import './EditableCell.css';

const Option = Select.Option;

/**
 * Clase para generar celdas editables, de forma dinámica, en función del tipo de elemento a editar.
 *
 * Props que recibe:
 * value: valor actual de la celda.
 * type: tipo del elemento editable a renderizar. Valores actuales: password, number, multicombo. Default: Input texto.
 * values: en caso de ser multicombo, todos los elementos seleccionables en el mismo para ese elemento. Algo así como la
 * store de todos los valores a cargar en el combo.
 * current: en caso de ser mutlicombo, valores actuales a mostrar en esa celda para ese registro.
 * onChange: método de gestión de cambios.
 *
 */
export default class EditableCell extends Component {
    state = {
        value: this.props.value,
        editable: false,
        emptyValue: this.props.emptyValue,
        edited: false,
        type: this.props.type,
        values: this.props.values,
        current: this.props.current,
        selected: ''
    };

    /**
     * Maneja el cambio de valores a la hora de editar.
     *
     * @param e event
     */
    handleChange = (e) => {
        this.setState({edited: true});
        let value;
        switch (this.state.type) {
            case 'multiCombo':
                let values = [];
                for (let i = 0; i < e.length; i++) {
                    values.push(e[i].key);
                }
                value = values.join(', ');
                break;
            case 'boolean':
                value = e;
                break;
            case 'number':
                value = e;
                break;
            default:
                value = e.target.value;
        }
        this.setState({value});
    };

    /**
     * Método de validación de edición. Llama al método de guardado de la clase padre.
     */
    check = () => {
        this.setState({editable: false});
        if (this.state.edited) {
            if (this.props.onChange) {
                this.props.onChange(this.state.value);
            }
        }
    };

    /**
     * Método de activación del modo edición.
     */
    edit = () => {
        this.setState({editable: true});
    };

    /**
     * Cancela la edición con la tecla Esc. No funciona en los multicombos.
     *
     * @param e event
     */
    handleKeyPress = (e) => {
        if (e.key == 'Escape') {
            this.cancelEdit();
        }
    };

    handleKeyPressNumber = (e) => {
        if (e.key == 'Escape') {
            this.cancelEdit();
        }
        if ((e.key == 'ArrowLeft') || (e.key == 'ArrowRight') ||
            (e.key == 'ArrowUp') || (e.key == 'ArrowDown') ||
            (e.key == 'Delete') || (e.key == 'Backspace') || (e.key == 'Enter')) {
            return;
        }
        if (e.key.search(/\d/) == -1) {
            e.preventDefault();
        }
    };


    /**
     *  Cancela el modo edición.
     */
    cancelEdit = () => {
        this.setState({editable: false});
    };

    /**
     * Devuelve un tipo de elemento de edición en función del type suministrado.
     *
     * @param type
     * @param value
     * @param values
     * @param current
     * @returns {*}
     */
    setEditType = (type, value, values, current) => {
        switch (type) {
            case 'password':
                return (
                    <div>
                        <Input
                            type="password"
                            value={value}
                            onChange={this.handleChange}
                            onPressEnter={this.check}
                            onKeyDown={this.handleKeyPress}
                        />
                        <Tooltip title={T.translate("common_confirm2")}>
                            <Icon
                                type="check"
                                className="editable-cell-icon-check"
                                onClick={this.check}
                            />
                        </Tooltip>
                    </div>
                );
            case 'number':
                return (
                    <div>
                        <InputNumber
                            value={value}
                            onChange={this.handleChange}
                            onPressEnter={this.check}
                            onKeyDown={this.handleKeyPressNumber}
                            min={0}
                        />
                        <Tooltip title={T.translate("common_confirm2")}>
                            <Icon
                                type="check"
                                className="editable-cell-icon-check"
                                onClick={this.check}
                            />
                        </Tooltip>
                    </div>
                );
            case 'boolean':
                return (
                    <div>
                        <Select
                            defaultValue={value == 1 ? '1':'0'}
                            style={{width: '100%'}}
                            onChange={this.handleChange}
                        >
                            <Option value="1">{T.translate("common_enabled")}</Option>
                            <Option value="0">{T.translate("common_disabled")}</Option>
                        </Select>
                        <Tooltip title={T.translate("common_confirm2")}>
                            <Icon
                                type="check"
                                className="editable-cell-icon-check"
                                onClick={() => this.check(values)}
                            />
                        </Tooltip>
                    </div>)
            case 'multiCombo':
                //Genera las opciones del combo en función de los values pasados.
                let options = [];
                for (let i = 0; i < values.length; i++) {
                    options.push(<Option key={values[i].id} value={values[i].id.toString()}>{values[i].name}</Option>)
                }
                //De haberlos, genera un array con los valores actuales del registro.
                let currentValues = [];
                for (let i = 0; i < current.split(', ').length; i++) {
                    currentValues.push({key: current.split(', ')[i]})
                }
                return (
                    <div>
                        <Select
                            labelInValue
                            defaultValue={currentValues[0].key != "" ? currentValues : []}
                            style={{width: '100%'}}
                            mode="multiple"
                            onChange={this.handleChange}
                        >
                            {options}
                        </Select>
                        <Tooltip title={T.translate("common_confirm2")}>
                            <Icon
                                type="check"
                                className="editable-cell-icon-check"
                                onClick={() => this.check(values)}
                            />
                        </Tooltip>
                    </div>);
            default:
                return (
                    <div>
                        <Input
                            value={value}
                            onChange={this.handleChange}
                            onPressEnter={this.check}
                            onKeyDown={this.handleKeyPress}
                        />
                        <Tooltip title={T.translate("common_confirm2")}>
                            <Icon
                                type="check"
                                className="editable-cell-icon-check"
                                onClick={this.check}
                            />
                        </Tooltip>
                    </div>
                );
        }
    };

    /**
     * Devuelve un valor a mostrar en función del type suministrado.
     *
     * @param type tipo de elemento.
     * @param value valor.
     * @param emptyValue bastante descriptivo. No vale de nada, pero por ahí anda de un copy-paste.
     * @returns {*} el elemento a renderizar.
     */
    setShowType = (type, value, emptyValue) => {
        switch (type) {
            case 'password':
                return (
                    <div>
                        {'••••••••'}
                        <Tooltip title={T.translate("common_edit")}>
                            <Icon
                                type="edit"
                                className="editable-cell-icon"
                                onClick={this.edit}
                            />
                        </Tooltip>
                    </div>
                );
            case 'switch':
                return (<Switch checked={value == 1} onChange={this.setActive(value)}/> );
            case 'boolean':
                return <div>
                    {value == '1' ? T.translate("common_enabled") : T.translate("common_disabled")}
                    <Tooltip title={T.translate("common_edit")}>
                        <Icon
                            type="edit"
                            className="editable-cell-icon"
                            onClick={this.edit}
                        />
                    </Tooltip>
                </div>
            case 'multiCombo':
                return (
                    <div>
                        {Array.isArray(value) ? value.join(', ') : value}&nbsp;
                        <Tooltip title={T.translate("common_edit")}>
                            <Icon
                                type="edit"
                                className="editable-cell-icon"
                                onClick={this.edit}
                            />
                        </Tooltip>
                    </div>
                );
            default:
                if(this.props.tableID && this.props.tableID=='profileConfig' && this.props.record && this.props.record.id == 1){
                    return (<div>
                        {value || emptyValue || ' '}
                    </div>);
                }else{
                    return (
                        <div>
                            {value || emptyValue || ' '}
                            <Tooltip title={T.translate("common_edit")}>
                                <Icon
                                    type="edit"
                                    className="editable-cell-icon"
                                    onClick={this.edit}
                                />
                            </Tooltip>
                        </div>
                    );
                }
        }
    };

    /**
     * A medio implementar. Planteado como método para manejar un switch, pero por ahora no hace falta.
     *
     * @param value
     */
    setActive = (value) => {

    };

    /**
     * Método de renderizado. Renderiza en función de si está o no activo el modo edición.
     *
     * @returns {*}
     */
    render() {
        const {value, editable, emptyValue, type, values, current} = this.state;
        return (
            <div className="editable-cell">
                {
                    editable ?
                        <div className="editable-cell-input-wrapper">
                            {this.setEditType(type, value, values, current)}
                        </div>
                        :
                        <div className="editable-cell-text-wrapper">
                            {this.setShowType(type, value, emptyValue)}
                        </div>
                }
            </div>
        );
    }
}