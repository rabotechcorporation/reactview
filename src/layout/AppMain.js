import React, { Component } from 'react';
import { Layout } from 'antd';
import { Route, Switch } from 'react-router-dom';
import { withRouter } from "react-router-dom";

import Log from '../views/Configuration/Log/Log'
import Unauthorized from '../views/Unauthorized';
import UserConfig from '../views/Configuration/Users/UserConfig';
import ProfileConfig from '../views/Configuration/Profiles/ProfileConfig';
import Configuration from '../views/Configuration';
import stringSimilarity from 'string-similarity';


const { Content } = Layout;

/**
 * Contenido principal.
 *
 * Aqui es necesario agregar todas las vistas principales posibles!
 */
class AppMain extends Component {

    componentDidMount() {
        this.checkValid(this.props);
    }
    componentWillReceiveProps(nextProps) { // or componentDidUpdate
        this.checkValid(nextProps);
    }


    checkValid = (props) => {
        //TODO check if menu has items
        let locationToCheck = props.location.pathname.split('/');

        if(Object.getOwnPropertyNames(props.app.state.user).length > 0){
            let exists = false;
            let sections = props.app.state.user.sections;

            for(let i in sections){
                let comparison = stringSimilarity.compareTwoStrings(sections[i].split('/')[1], locationToCheck[1]);
                if(comparison > 0.8){
                    exists = true;
                    break;
                }
            }

            if (!exists && props.location.pathname !== '/403' && props.location.pathname !== '/') {
                this.props.history.replace('/403');
            }
        }

    };

    render() {
        const defaultProps = {
            app: this.props.app
        };

        return (
            <Content style={{ background: '#fff', padding: 24, paddingTop: 0, margin: 0, minHeight: 280}}>
                <Switch>
                    <Route exact path='/'  render={({ match }) => <Configuration match={match}  {...defaultProps}  />} />
                    <Route exact path='/403' render={({ match }) => <Unauthorized match={match} {...defaultProps} />} />
                    <Route exact path='/configuration' render={({ match }) => <Configuration match={match}  {...defaultProps} />} />
                    <Route exact path='/configuration/log'  render={({ match }) => <Log match={match}  {...defaultProps}  />} />
                    <Route exact path='/configuration/users' render={({ match }) => <UserConfig match={match}  {...defaultProps} />} />
                    <Route exact path='/configuration/profiles' render={({ match }) => <ProfileConfig match={match}  {...defaultProps} />} />
                </Switch>
            </Content >
        );
    }
}

export default withRouter(AppMain)
