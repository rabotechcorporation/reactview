import React, {Component} from 'react';
import {Layout, Row, Col, Button, Modal, Popover, Icon, Affix} from 'antd';
import LoginHandler from '../common/LoginHandler';
import LoginModal from '../common/LoginModal';
import LandraBreadcrumb from '../components/LandraBreadcrumb/LandraBreadcrumb';
import ComboBox from '../components/ComboBox';
import GlobalSearch from '../components/GlobalSearch';
import axios from 'axios';
import T from "i18n-react/dist/i18n-react";

const confirm = Modal.confirm;
const {Header} = Layout;

let loginHandler = new LoginHandler();


export default class AppHeader extends Component {
    state = {
        modalWindowVisible: false,
        modalStatusLoading: false,
        location: {}
    };

    constructor(props) {
        super(props);
        this.app = this.props.app;
    }

    /**
     * Metodo que refresca el contenido del popover con la informacion del usuario
     */
    userInfoContent = () => {
        if (this.props.user) {
            return (
                <div>
                    <b>{T.translate("login_username")}: </b> {this.props.user.username}<br />
                    <b>{T.translate("login_profile")}: </b> {this.props.user.name}
                </div>
            )
        }
        return "";
    };

    /**
     * Wrapper que realiza el login
     */
    login = (values) => {
        const self = this;
        loginHandler.login(this, values, function () {
            self.props.userLoaded();
            self.props.logout(false);
        });
    };

    /**
     * Wrapper para confirmar el cierre de sesion
     */
    logout = () => {
        const self = this;
        confirm({
            title: T.translate('login_logoutTitle'),
            content: T.translate('login_logoutMessage'),
            okText: T.translate('common_yes'),
            okType: 'danger',
            cancelText: T.translate('common_no'),
            onOk() {
                loginHandler.logout(self);
                self.props.logout(true);
            },
            onCancel() {
                console.log('Cancel');
            },
        });
    };

    /**
     * Metodo que muestra la ventana modal
     */
    showLoginModal = () => {
        this.setState({
            modalWindowVisible: true,
        });
    };

    /**
     * Evento ejecutado al cargar el header y comprobar si la sesion esta activa o no
     *  {this.context.intl.formatMessage(messages.test)}
     */
    componentDidMount() {
        const self = this;
        self.props.userLoaded();
        loginHandler.checkUser(this, function () {
            self.props.userLoaded();
        });
        window.addEventListener("resize", this.updateDimensions);
    };


    unauthorized = () => {
        if(!this.state.modalWindowVisible){
            this.showLoginModal();
            this.props.logout(true);
        }
    };

    updateDimensions = () => {
        this.setState({width: window.innerWidth, height: window.innerHeight});
    };
    componentWillMount() {
        this.updateDimensions();
    };
    componentWillUnmount() {
        window.removeEventListener("resize", this.updateDimensions);
    };

    render() {

        return (
            <Affix>
                <Header style={{background: '#fff', padding: '0'}}>
                    <Row >
                        <Col xs={12} sm={5} md={4} lg={4} xl={3}>
                            <div className="logo">
                                {window.innerWidth > 1200 ? <img alt="" height="40px" style={{marginTop: 10, marginBottom: 10}} src={process.env.PUBLIC_URL + '/logo.png'}/> : <img alt="" height="20px" style={{marginTop: 10, marginBottom: 10}} src={process.env.PUBLIC_URL + '/logo.png'}/>}
                            </div>
                        </Col>
                        {window.innerWidth > 1200 ?
                            <Col xs={0} sm={5} md={6} lg={8} xl={9} style={{position: 'absolute', left:'225px'}}>
                                <LandraBreadcrumb position={this.state.location}/>
                            </Col>
                            :
                            <Col xs={0} sm={5} md={6} lg={8} xl={9} style={{position: 'absolute', left:'110px'}}>
                                <LandraBreadcrumb position={this.state.location}/>
                            </Col>
                        }

                        <Col>
                            <div className="topHeaderButtons">
                                <Button  icon="logout" onClick={this.logout}/>
                                <Popover content={this.userInfoContent()} title={T.translate("user_panel_title")}
                                         placement="bottomRight" trigger="click">
                                    <Button icon="user">{this.props.user ? this.props.user.username : "-"}</Button>
                                </Popover>
                                <LoginModal title={T.translate("login_title")} visible={this.state.modalWindowVisible}
                                            loading={this.state.modalStatusLoading} handleOk={this.login}/>
                            </div>
                        </Col>
                    </Row>
                </Header>
            </Affix>
        );
    }
}
