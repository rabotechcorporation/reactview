import React, { Component } from 'react';
import { Menu, Icon } from 'antd';
import { Link } from 'react-router-dom';
import axios from 'axios';
import T from "i18n-react/dist/i18n-react";
import lodash from 'lodash';
import menu from "../menu"

const { SubMenu } = Menu;



export default class AppMenu extends Component {
    state = {
        menu: menu
    };

    menuDefault = [
        {
            //icon : 'tool',
            icon : 'setting',
            label : 'configuration',
            url : '/configuration'
        }
    ];

    drawMenuItem = (item) => {
        if (item.children) {
            return this.drawSubMenu(item);
        }
        return (
            <Menu.Item key={item.url}>
                <Icon type={item.icon} />
                <Link to={item.url} style={{ display: 'inline' }}><span>{item.label}</span></Link>
            </Menu.Item>
        )
    };

    drawSubMenu = (item) => {
        let submenu = [];
        for (let idx in item.children) {
            let child = item.children[idx];
            submenu.push(this.drawMenuItem(child));
        }
        return (
            <SubMenu key={item.label + "subm"} title={<span><Icon type={item.icon} /><span className="nav-text">{item.label}</span></span>}>
                {submenu}
            </SubMenu>
        )
    };

    drawMenu = (menu) => {
        let menuElements = [];
        if (!menu) menu = this.menuDefault;
        for (let idx in menu) {
            let item = menu[idx];
            menuElements.push(this.drawMenuItem(item));
        }
        return menuElements;
    };

    loadMenuFromServer = () => {
        this.setState({
            menu: menu
        });
        /*if (this.props.user) {
            axios.get('/status_control/menu/', {
                withCredentials: true
            }).then((data) => {
                this.setState({
                    menu: data.data.data
                });
            });
        }*/
    };

    componentDidUpdate(prevProps, prevState) {
        if (prevProps.user !== this.props.user) {
            this.loadMenuFromServer();
        }
    }

    render() {
        let active = window.location.pathname;

        let found = lodash.find(this.state.menu, {url: window.location.pathname});
        //Si no lo encuentra se supone que es una subruta y se coge la primera parte
        if(!found){
            active = active.substring(0, active.lastIndexOf("/"));
        }

        return (
            <div>
                <Menu theme="light" mode="inline" style={{ height: "88vh", marginTop: 10 }} selectedKeys={[active]}>
                    {this.drawMenu(this.state.menu)}
                </Menu>
            </div>
        );
    }
}
