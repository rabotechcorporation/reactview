import {LocaleProvider} from 'antd';
import {addLocaleData, IntlProvider} from 'react-intl';
import React, {Component} from 'react';
import { withRouter } from "react-router-dom";
import {Layout, Affix} from 'antd';
import config from 'react-global-configuration';
import async from 'async';

import configuration from './config';
import AppMain from './layout/AppMain'
import AppHeader from './layout/AppHeader'
import AppMenu from './layout/AppMenu'
import AxiosConfig from './common/AxiosConfig'
import TranslationLoader from './common/TranslationLoader'
import localeData from './locales/es-ES.js'


import './App.css';
import ConfigHandler from './common/ConfigHandler';
import Logout from "./views/Logout";

const {Sider, Footer} = Layout;

addLocaleData(localeData.data);


class App extends Component {
    state = {
        user: {},
        loaded: false,
        logout: false,
        userLoaded: false
    };

    constructor(props) {
        super(props);
        const self = this;
        //Load global configuration
        config.set(configuration, {freeze: false});
        //Set axios global parameters
        AxiosConfig(this);

        async.series([
                function (callback) {
                    TranslationLoader.loadTranslations(callback);
                }],
            function () {
                self.setState({
                    loaded: true
                })
            });
    }

    setCurrentBreadcrumbs = function (location) {
        this.appHeader.setState({
            location: location
        });
    };

    setLogout = (status) => {
        if(status){
            this.setState({logout: true});
        }else{
            this.setState({logout: false});
        }
    };

    userLoaded = () => {
        this.props.history.push({
            pathname: "/"
        });
        this.setState({userLoaded: true})
    };

    render() {
        // console.log(this.state.logout)
        return (
            <div>
                {this.state.loaded == true ?
                    <LocaleProvider locale={localeData.antd}>
                        <IntlProvider locale={localeData.locale}>
                            <div className="App">
                                <Layout >
                                    <AppHeader app={this} logout={this.setLogout} user={this.state.user} ref={(elm) => {
                                        this.appHeader = elm;
                                    }} userLoaded={this.userLoaded}/>
                                    <Layout>
                                        <Sider style={{background: '#fff', width: 240}} breakpoint="xl"
                                               collapsible="true">
                                            <Affix offsetTop={64}>
                                                <AppMenu app={this} user={this.state.user} ref={(elm) => { this.appMenu = elm; }}/>
                                            </Affix>
                                        </Sider>
                                        <Layout>
                                            {this.state.logout == true ? <Logout/> : this.state.userLoaded ? <AppMain app={this} /> : ''}
                                        </Layout>
                                    </Layout>
                                </Layout>
                            </div>
                        </IntlProvider>
                    </LocaleProvider> : <div/>
                }
            </div>
        );
    }
}

export default withRouter(App);
