import { notification } from 'antd';
import axios from 'axios';
import config from 'react-global-configuration';
import URL from 'url-parse';


/**
 * Se encarga de configurar el interceptor para los errores en las llamadas AJAX realizadas
 */
function ConfigureAxios(app) {
    let myUrl = new URL(window.location.href);
    axios.defaults.baseURL = myUrl.protocol + "//" + myUrl.hostname + ':' + config.get('serverPort');

    // Add a request interceptor
    axios.interceptors.response.use(function (response) {
        switch (response.data.status) {
            case 401:
                console.log(response.data.message);
                break;
            case 402:
                notification.error({
                    message: 'Error validación',
                    description: response.data.message
                })

                break;
            case 403:
                app.appHeader.unauthorized();
                break;
            case 500:
                notification.error({
                    message: 'Error desconocido',
                    description: response.data.message
                })
                break;

            case null:
            case "":
            case undefined:
            case "undefined":
            default:
                break;
        }
        // Do something before request is sent
        return response;
    }, function (error) {
        // Do something with request error
        notification.error({
            message: 'Error Desconocido',
            description: "Se ha producido un error en la llamada al servidor"
        })
        return Promise.reject(error);
    });

}

export default ConfigureAxios;