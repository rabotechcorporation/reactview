import { message } from 'antd';
import axios from 'axios';

/**
 * Clase encargada de gestionar los login, las comprobaciones y obtenciones de datos de usuario y los logout
 */
export default class LoginHandler {
    /**
     * Comprueba si el usuario esta logueado y si lo esta almacena sus datos
     */
    checkUser = (relatedComponent, callback) => {
        axios.get('/getUser',
            {
                withCredentials: true
            }).then((data) => {
                relatedComponent.app.setState({
                    user: data.data.data
                });

                if (data.data.status === 403) {
                    relatedComponent.showLoginModal();
                    return;
                }
                if(callback) callback();
            });
    };

    /**
     * Realiza el inicio de sesion segun los parametros obtenidos
     */
    login = (relatedComponent, params, callback) => {
        relatedComponent.setState({
            modalStatusLoading: true
        });
        axios.post('/login', params,
            {
                withCredentials: true
            }).then((data) => {
                relatedComponent.setState({
                    modalStatusLoading: false
                });
                if (data.data.success) {
                    relatedComponent.app.setState({
                        user: data.data.data
                    });
                    relatedComponent.setState({
                        modalWindowVisible: false
                    });
                    message.success('Se ha iniciado sesión como ' + data.data.data.username + '.');
                    if(callback) callback();
                } else {
                    message.error('Usuario o contraseña incorrectos.');
                }
            });
    };

    /**
     * Realiza el cierre de la sesion actual
     */
    logout = (relatedComponent) => {
        axios.get('/logout',
            {
                withCredentials: true
            }).then((data) => {
                this.checkUser(relatedComponent);
            });
    }
}