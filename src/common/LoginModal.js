import React, { Component } from 'react';
import { Button, Modal, Form, Icon, Input } from 'antd';
import ComboBox from '../components/ComboBox';
import T from "i18n-react/dist/i18n-react";

/**
 * Formulario de login. Definicion del formulario y sus validaciones
 */
class NormalLoginForm extends React.Component {

    render() {
        const { getFieldDecorator } = this.props.form;
        return (
            <Form className="login-form">
                <Form.Item>
                    {getFieldDecorator('user', {
                        rules: [{ required: true, message: 'El usuario es obligatorio' }],
                    })(
                        <Input prefix={<Icon type="user" style={{ fontSize: 13 }} />} placeholder="Nombre de Usuario"
                            onPressEnter={this.props.onSubmit} onChange={this.props.userChange} />
                        )}
                </Form.Item>
                <Form.Item>
                    {getFieldDecorator('pass', {
                        rules: [{ required: true, message: 'La contraseña es obligatoria' }],
                    })(
                        <Input prefix={<Icon type="lock" style={{ fontSize: 13 }} />} type="password"
                            placeholder="Contraseña" onPressEnter={this.props.onSubmit} />
                        )}
                </Form.Item>
            </Form>
        );
    }
}
/**
 * Constructor para el formulario
 */
const LoginForm = Form.create()(NormalLoginForm);

export default class LoginModal extends Component {

    state = {
        username: ""
    };

    /**
     * Metodo que obtiene el formulario segun su referencia y lo valida al hacer click en el boton aceptar
     */
    handleClick = () => {
        const self = this;
        this.loginForm.validateFields((err, values) => {
            if (err) {
                return console.log(err);
            }
            this.props.handleOk(values);
            this.loginForm.resetFields();
        });

    };

    userChange = (e) => {
        this.setState({ username: e.target.value });
    };

    render() {
        return (

            <Modal
                visible={this.props.visible}
                title={this.props.title}
                onOk={this.props.handleOk}
                closable={false}
                wrapClassName="vertical-center-modal"
                footer={[
                    <Button key="submit" type="primary"
                            size="large" loading={this.props.loading}
                            onClick={this.handleClick}
                            id="btn_login_ok"
                    >
                        {T.translate("common_button_accept")}
                    </Button>,
                ]}
            >
                <LoginForm ref={(form) => {
                    this.loginForm = form;
                }}  onSubmit={this.handleClick}  userChange={this.userChange} />
            </Modal>
        );
    }
}