import axios from 'axios';
import T from "i18n-react/dist/i18n-react";


export default {

    actualData: {},

    loadTranslations: function (callback) {
        const self = this;
        window.translate = function (textId) {
            console.error('Deprecated translation method. Use T.translate() from \'i18n-react\' module instead.');
            let rec = self.actualData[textId];
            return rec ? rec : "undefined." + textId;
        };

        axios.get('/translation', {withCredentials: true})
            .then((data) => {
                T.setTexts(data.data.data);
                self.actualData = data.data.data;
                if (callback) callback();
            });
    }
}