import axios from 'axios';

export default {

    data: {},

    loadConfiguration: function(callback) {
        const self = this;
        axios.get('/settings/load', {withCredentials: true})
            .then((data) => {
                self.data = data.data.data;
                if (callback) callback();
            });
    },

    updateConfiguration: function (record) {
        this.data[record.cfg_key] = record.value;
        console.log(this.data)
    },

    getConfigParam: function(param) {
        if(param){
            return this.data[param];
        }
        return this.data;
    }
    // loadTranslations: function (callback) {
    //     const self = this;
    //     window.translate = function (textId) {
    //         console.error('Deprecated translation method. Use T.translate() from \'i18n-react\' module instead.');
    //         let rec = self.actualData[textId];
    //         return rec ? rec : "undefined." + textId;
    //     };
    //
    //     axios.get('/translation', {withCredentials: true})
    //         .then((data) => {
    //             T.setTexts(data.data.data);
    //             self.actualData = data.data.data;
    //             if (callback) callback();
    //         });
    // }
}