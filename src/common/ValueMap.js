import T from "i18n-react/dist/i18n-react";

export function mapValue(value) {
    let mappedValue = '';
    switch (value) {
        case 'created':
            mappedValue = T.translate('order_demogstatus_created');
            break;
        case 'correct':
            mappedValue = T.translate('order_demogstatus_correct');
            break;
        case 'not_correct':
            mappedValue = T.translate('order_demogstatus_incorrect');
            break;
        case 'uncheck':
            mappedValue = T.translate('order_demogstatus_uncheck');
            break;
        case 'ANALISIS':
            mappedValue = 'AT. Primaria';
            break;
        case 'ANALISISURG':
            mappedValue = 'Urgencias';
            break;
        case 'ANALISISHOSP':
            mappedValue = 'Hospitalización';
            break;
        case 'ANALISISCE':
            mappedValue = 'Consultas Externas';
            break;
        case 'wcom':
            mappedValue = 'Con comentarios';
            break;
        case 'wocom':
            mappedValue = 'Sin comentarios';
            break;
        case 'comrd':
            mappedValue = 'Comentarios leídos';
            break;
        case 'comnrd':
            mappedValue = 'Comentarios no leídos';
            break;
        case 'sent':
            mappedValue = 'Enviada';
            break;
        case 'sending':
            mappedValue = 'Enviado Petición';
            break;
        case 'blocked':
            mappedValue = 'Retenida';
            break;
        case 'blocked_tests':
            mappedValue = 'Pruebas Retenidas';
            break;
        case 'processing':
            mappedValue = 'Procesando...';
            break;
        case 'pending':
            mappedValue = 'Pendiente';
            break;
        case 'error':
            mappedValue = 'Error Comunicación';
            break;
        case 'unknown':
            mappedValue = 'Desconocido';
            break;
        case 'name':
            mappedValue = 'Nombre';
            break;
        case 'surname':
            mappedValue = 'Primer apellido';
            break;
        case 'secondsurname':
            mappedValue = 'Segundo apellido';
            break;
        case 'second_surname':
            mappedValue = 'Segundo apellido';
            break;
        case 'doctor':
            mappedValue = 'Doctor';
            break;
        case 'orderIdOrigin':
            mappedValue = 'Pet. Origen';
            break;
        case 'orderIdDestination':
            mappedValue = 'Pet. Destino';
            break;
        case 'birthdate':
            mappedValue = 'Fecha de nacimiento';
            break;
        case 'sex':
            mappedValue = 'Sexo';
            break;
        case 'destination':
            mappedValue = 'Destino';
            break;
        case 'origin':
            mappedValue = 'Origen';
            break;
        case 'service':
            mappedValue = 'Servicio';
            break;
        case 'status':
            mappedValue = 'Estados';
            break;
        case 'com':
            mappedValue = 'Comunicación';
            break;
        case 'lis':
            mappedValue = "LIS";
            break;
        case 'wi':
            mappedValue = "Con Incidencias";
            break;
        case 'woi':
            mappedValue = "Sin Incidencias";
            break;
        default:
            mappedValue = value;
    }
    return mappedValue;
}