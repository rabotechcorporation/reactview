import React, { Component } from "react";
import { Table, notification, Popconfirm, Button, Tooltip } from "antd";
import axios from "axios";
import EditableCell from "../../../components/EditableCell/EditableCell";
import TaggedFilter from "../../../components/TaggedFilter/TaggedFilter";
import T from "i18n-react/dist/i18n-react";
import ConfigHandler from '../../../common/ConfigHandler';

export default class SettingsConfig extends Component {

    constructor(props) {
        super(props);
        this.app = this.props.app;
    }

    state = {
        data: [],
        pagination: {},
        loading: false,
        selectedRowKeys: [],
        addInputValue: "",
        modal: false,
        record: '',
        filters: {}
    };

    tableProps = {
        tableIdentifier: 'settingsConfig',
        bordered: true,
        loading: false,
        pagination: true,
        size: 'small',
        rowKey: "id"
    };

    columns = [
        {
            title: T.translate("config_settings_table_name"),
            dataIndex: 'name',
            key: 'name',
            width: '20%',
            filter: true,
            render: (text, record) => (
                <Tooltip title={record.cfg_key}>
                    {text}
                </Tooltip>
            )
        },
        {
            title: T.translate("config_settings_table_value"),
            dataIndex: 'cfg_value',
            key: 'cfg_value',
            width: '10%',
            render: (text, record) => (
                <EditableCell
                    record={record}
                    tableID={this.tableProps.tableIdentifier}
                    type={record.value_type}
                    value={text}
                    onChange={this.onCellChange(record.id, 'value')}
                />
            )
        },
        {
            title: T.translate("config_settings_table_description"),
            dataIndex: 'description',
            key: 'description',
            width: '70%',
            filter: true,
            render: (text, record) => (
                <Tooltip title={T.translate(text)}>
                    {text}
                </Tooltip>
            )
        }
    ];

    onCellChange = (key, dataIndex) => {
        return (value) => {
            const dataSource = [...this.state.data];
            const target = dataSource.find(item => item.id === key);
            if (target) {
                target[dataIndex] = value;
                this.saveRecord(target);
            }
        };
    };

    saveRecord = (record) => {
        axios.post('/settings/' + record.id, record, {
            withCredentials: true
        }).then((data) => {
            if (data.data.success) {
                console.log(record)
                ConfigHandler.updateConfiguration(record);
                this.search();
                notification.info({
                    message: T.translate("config_profiles_update"),
                    description: T.translate("config_profiles_update_success_message")
                });
            } else {
                notification.error({
                    message: T.translate("common_error_update"),
                    description: T.translate("config_profiles_update_error_message")
                });
            }
        })
    };

    search = (params = {}) => {
        this.setState({
            loading: true,
            data: []
        });
        axios.get('/settings/list', {
            withCredentials: true,
            params: params
        }).then((data) => {
            const pagination = { ...this.state.pagination };
            // Read total count from server
            pagination.total = data.data.total;
            pagination.showSizeChanger = true;
            this.setState({
                loading: false,
                data: data.data.data,
                pagination,
            });
        });
    };



    restartMsgServer = () => {

        axios.get('/status_control/restartMsgServer', {
            withCredentials: true
        }).then((data) => {

            notification.info({
                message: 'Reinicio Completado',
                description: data.data.data
            });

        });

    }


    componentDidMount = () => {
        this.search();
        this.app.setCurrentBreadcrumbs({
            "ilp": false,
            "configuration": "/configuration",
            "config_settings": true//"/configuration/profiles",
        });
    };


    // onSelectChange = (selectedRowKeys) => {
    //     this.setState({
    //         selectedRowKeys: selectedRowKeys
    //     });
    //
    // };

    rowClick = (record, index, event) => {
        this.setState({
            selectedRowKeys: [record.id]
        });
    };

    render() {
        // const rowSelection = {
        //     type: 'radio',
        //     onChange: this.onSelectChange,
        //     selectedRowKeys: this.state.selectedRowKeys
        // };
        return (
            <div>
                {/*<TaggedFilter columns={this.columns} fetchFunction={this.search}/>*/}
                <h2>{T.translate("config_settings_title")}</h2>

                <Popconfirm title={T.translate("common_question")} onConfirm={() => { this.restartMsgServer() }}>
                    <Button type="primary" style={{ float: 'right' }} >Reiniciar Servidor Mensajeria</Button>
                </Popconfirm>
                <br />
                <br />
                <TaggedFilter
                    parent={this}
                    columns={this.columns}
                    fetchFunction={this.search}
                    currentFilters={this.state.filters}
                />
                <br />
                <br />
                <Table {...this.tableProps}
                    // rowSelection={rowSelection}
                    dataSource={this.state.data}
                    pagination={this.state.pagination}
                    loading={this.state.loading}
                    onRow={(record, index) => ({
                        onClick: () => {
                            this.rowClick(record, index)
                        },
                    })}
                    columns={this.columns} />
            </div>
        );
    }
}