import React, {Component} from "react";
import {Button, Input, Tabs} from "antd";
import axios from "axios";
import T from "i18n-react/dist/i18n-react";

export default class Log extends Component {

    constructor(props) {
        super(props);
        this.app = this.props.app;
    }

    state = {
        infoData: [],
        errorData: [],
        messagesData: [],
        performanceData: [],
        activeTab: '1'
    };

    search = () => {
        switch(this.state.activeTab){
            case '1':
                this.setState({
                    infoData: []
                });
                axios.get('/log_err', {
                    withCredentials: true
                }).then((data) => {
                    if (data.data.data.substring(0, 1) === ',') {
                        data.data.data = data.data.data.substring(1);
                    }
                    this.setState({
                        infoData: data.data.data
                    });
                });
                break;
            // case '2':
            //     this.setState({
            //         errorData: []
            //     });
            //     axios.get('/log_error', {
            //         withCredentials: true
            //     }).then((data) => {
            //         this.setState({
            //             errorData: data.data.data
            //         });
            //     });
            //     break;
            // case '3':
            //     this.setState({
            //         messagesData: []
            //     });
            //     axios.get('/log_messages', {
            //         withCredentials: true
            //     }).then((data) => {
            //         this.setState({
            //             messagesData: data.data.data
            //         });
            //     });
            //     break;
            // case '4':
            //     this.setState({
            //         performanceData: []
            //     });
            //     axios.get('/log_performance', {
            //         withCredentials: true
            //     }).then((data) => {
            //         this.setState({
            //             performanceData: data.data.data
            //         });
            //     });
            //     break;
        }
        this.scrollToBottom();
    };

    componentDidMount = () => {
        this.search();
        this.app.setCurrentBreadcrumbs({
            "ilp": false,
            "configuration": "/configuration",
            "log": true//"/configuration/log",
        });
        this.scrollToBottom();
    };

    componentDidUpdate() {
        this.scrollToBottom();
    };

    scrollToBottom() {
        if(this.infoArea){
            this.infoArea.textAreaRef.scrollTop = this.infoArea.textAreaRef.scrollHeight;
        }
        // if(this.errorArea){
        //     this.errorArea.textAreaRef.scrollTop = this.errorArea.textAreaRef.scrollHeight;
        // }
        // if(this.messagesArea){
        //     this.messagesArea.textAreaRef.scrollTop = this.messagesArea.textAreaRef.scrollHeight;
        // }
        // if(this.performanceArea){
        //     this.performanceArea.textAreaRef.scrollTop = this.performanceArea.textAreaRef.scrollHeight;
        // }
    }

    activeTab = (e) => {
        // this.setState({activeTab: e}, function () {
        //     this.search()
        // });
    };

    render() {
        return (
            <div>
                <h2>{T.translate("config_log_title")}</h2>
                <br/>
                 <Button type="primary" onClick={this.search}>{T.translate("config_log_refresh")}</Button>
                <br/>
                <br/>
                <Tabs defaultActiveKey={this.state.activeTab} onChange={this.activeTab}>
                    <Tabs.TabPane tab={T.translate("config_log_info")} key="1">
                        <Input.TextArea style={{cursor: 'auto' , height: '66vh', maxHeight: '66vh'}}
                                        value={this.state.infoData}
                                        ref={infoArea => { this.infoArea = infoArea; }}
                        />
                    </Tabs.TabPane>
                    {/*<Tabs.TabPane tab={T.translate("config_log_errors")} key="2">*/}
                        {/*<Input.TextArea style={{cursor: 'auto', height: '66vh', maxHeight: '66vh'}}*/}

                                        {/*value={this.state.errorData}*/}
                                        {/*ref={errorArea => { this.errorArea = errorArea; }}*/}
                        {/*/>*/}
                    {/*</Tabs.TabPane>*/}
                    {/*<Tabs.TabPane tab={T.translate("config_log_messages")} key="3">*/}
                        {/*<Input.TextArea style={{cursor: 'auto', height: '66vh', maxHeight: '66vh'}}*/}

                                        {/*value={this.state.messagesData}*/}
                                        {/*ref={messagesArea => { this.messagesArea = messagesArea; }}*/}
                        {/*/>*/}
                    {/*</Tabs.TabPane>*/}
                    {/*<Tabs.TabPane tab={T.translate("config_log_performance")} key="4">*/}
                        {/*<Input.TextArea style={{cursor: 'auto', height: '66vh', maxHeight: '66vh'}}*/}

                                        {/*value={this.state.performanceData}*/}
                                        {/*ref={performanceArea => { this.performanceArea = performanceArea; }}*/}
                        {/*/>*/}
                    {/*</Tabs.TabPane>*/}
                </Tabs>
            </div>
        );
    }
}