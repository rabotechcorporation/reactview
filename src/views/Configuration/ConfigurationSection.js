import React, {Component} from 'react';

import {Link} from 'react-router-dom';
import {Icon, Card, Row, Col} from 'antd';
import T from "i18n-react/dist/i18n-react";


export default class ConfigurationSection extends Component {


    create_links = (list) => {

        let html = [];

        for (let name in list)
            html.push(
                <div key={list[name]}>
                    <Link to={list[name]} >{T.translate("menu_config_"+name)}</Link>
                </div>
            );

        return html;
    };

    render() {
        return (
            <Card style={{
                margin: "20px",
                width: "15%",
                minWidth: 250,
                height: 120,
                float: "left"
            }}>
                <Row type={"flex"} gutter={16} align={"middle"} justify={"space-around"}>
                    <Col span={8}><Icon type={this.props.icon} style={{
                        fontSize: "60px",
                        fontWeight: 900
                    }}/></Col>
                    <Col span={16}>{this.create_links(this.props.links)}</Col>
                </Row>
            </Card>
        );
    };

};