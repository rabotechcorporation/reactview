import React, { Component } from "react";
import { notification, Button, Icon, Popover, Tooltip, Card } from "antd";
import axios from "axios";
import T from "i18n-react/dist/i18n-react";

import GenericAddForm from '../../../components/Form/GenericAddForm'
import TaggedFilter from "../../../components/TaggedFilter/TaggedFilter";
import EditableTable from "../../../components/EditableTable/EditableTable";
import ProfSecModal from "./ProfileSectionModal";

export default class ProfileConfig extends Component {

    constructor(props) {
        super(props);
        this.app = this.props.app;
        this.handleAdd = this.handleAdd.bind(this);
    }

    state = {
        data: [],
        pagination: {},
        loading: false,
        selectedRowKeys: [],
        addInputValue: "",
        modal: false,
        record: '',
        filters: {}
    };

    tableProps = {
        tableIdentifier: 'profileConfig',
        bordered: true,
        loading: false,
        pagination: true,
        size: 'small',
        rowKey: "id"
    };

    columns = [
        {
            title: T.translate("config_profiles_name"),
            dataIndex: 'name',
            key: 'name',
            filter: true,
            filterId: 'filterName'
        }
    ];

    actionColumns = [
        {
            dataIndex: 'profiles',
            width: 50,
            render: (text, record) => {
                return (
                     <div className="table-action-column">
                        <Tooltip title={T.translate("config_profiles_permissions")}>
                            <Icon type="profile" className="editable-table-icon" style={{ marginRight: 3 }} onClick={() => this.showModal(record)} />
                        </Tooltip>
                    </div> 
                )
            },
        }
    ];

    saveRecord = (record) => {
        axios.post('/profile/' + record.id, record, {
            withCredentials: true
        }).then((data) => {
            if (data.data.success) {
                this.search();
                notification.info({
                    message: T.translate("config_profiles_update"),
                    description: T.translate("config_profiles_update_success_message")
                });
            } else {
                switch (data.data.message.routine) {
                    case '_bt_check_unique':
                        notification.error({
                            message: T.translate("common_error"),
                            description: T.translate("config_profiles_unique_error_message")
                        });
                        break;
                    default:
                        notification.error({
                            message: T.translate("common_error_update"),
                            description: T.translate("config_profiles_update_error_message")
                        });
                }
            }
        })
    };

    addOverContent = () => {
        return (
            <Card>
                <GenericAddForm fields={['name']}
                    fieldNames={[T.translate("config_profiles_name")]}
                    types={['text']}
                    post={'/profile'}
                    required={[true]}
                    handleAdd={this.handleAdd}
                    layout={'inline'} />
            </Card>
        )
    };

    handleAdd = (data) => {
        if (data.success) {
            this.search();
            notification.info({
                message: T.translate("config_profiles_add"),
                description: T.translate("config_profiles_add_success_message")
            });
            this.setState({visibleForm: false})
        } else {
            switch (data.message.routine) {
                case '_bt_check_unique':
                    notification.error({
                        message: T.translate("common_error"),
                        description: T.translate("config_profiles_unique_error_message")
                    });
                    break;
                default:
                    notification.error({
                        message: T.translate("common_error"),
                        description: T.translate("config_profiles_add_error_message")
                    });
            }
        }
    };

    deleteRecord = (id) => {
        axios.post('/profile/delete/' + id, {}, {
            withCredentials: true
        }).then((data) => {
            if (data.data.success) {
                this.search();
                notification.info({
                    message: T.translate("config_profiles_del"),
                    description: T.translate("config_profiles_del_success_message")
                });
            } else {
                if (data.data.message && data.data.message.constraint == "fk_profile_has_user_profile1") {
                    notification.error({
                        message: T.translate("config_profiles_del_error_used_title"),
                        description: T.translate("config_profiles_del_error_used")
                    });
                } else {

                    notification.error({
                        message: T.translate("common_error"),
                        description: T.translate("config_profiles_del_error_message")
                    });
                }
            }
        })
    };

    search = (params = {}) => {
        this.setState({
            loading: true,
            data: []
        });
        axios.get('/profile/list', {
            withCredentials: true,
            params: params
        }).then((data) => {
            const pagination = { ...this.state.pagination };
            // Read total count from server
            pagination.total = data.data.total;
            pagination.showSizeChanger = true;
            this.setState({
                loading: false,
                data: data.data.data,
                pagination,
            });
        });
    };
    componentDidMount = () => {
        this.search();
        this.app.setCurrentBreadcrumbs({
            "ilp": false,
            "configuration": "/configuration",
            "config_profiles": true//"/configuration/profiles",
        });
    };

    showModal = (record) => {
        this.setState({ modal: true });
        this.setState({ record: record });
        this.child.parentCall(record);
    };

    hideModal = () => {
        this.setState({ modal: false });
    };

    showForm = () => {
        this.setState({visibleForm: true})
    };

    render() {
        // const rowSelection = {
        //     type: 'radio',
        //     onChange: this.onSelectChange,
        //     selectedRowKeys: this.state.selectedRowKeys
        // };
        return (
            <div>
                {/*<TaggedFilter columns={this.columns} fetchFunction={this.search}/>*/}
                <h2>{T.translate("config_profiles_title")}</h2>
                <br />
                <TaggedFilter
                    parent={this}
                    tableIdentifier="ProfileConfig"
                    columns={this.columns}
                    fetchFunction={this.search}
                    currentFilters={this.state.filters}
                />
                <Button type="primary" id="btn_add" onClick={this.showForm}>{T.translate("common_button_add")}</Button>
                <br />
                <br />
                <EditableTable
                    tableID={'ProfileConfig'}
                    dataSource={this.state.data}
                    pagination={this.state.pagination}
                    loading={this.state.loading}
                    columns={this.columns}
                    saveRecord={this.saveRecord}
                    deleteRecord={this.deleteRecord}
                    cancelEdit={() => this.search(this.state.filters)}
                    actionColumns={this.actionColumns}
                />
                {this.state.visibleForm === true && this.addOverContent()}
                <ProfSecModal
                    ref={instance => {
                        this.child = instance;
                    }}
                    show={this.state.modal}
                    hide={this.hideModal}
                    record={this.state.record}
                />
            </div>
        );
    }
}