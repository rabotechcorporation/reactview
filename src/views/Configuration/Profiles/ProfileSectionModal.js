import React, { Component } from 'react';
import axios from "axios";
import { Button, Modal, Row, Col, Checkbox, Spin } from 'antd';
import { notification } from "antd/lib/index";
import T from "i18n-react/dist/i18n-react";
import menu from '../../../menu.js';

class ProfSecModal extends Component {

    state = {
        record: this.props.record,
        allSections: [],
        profileSections: [],
        group: [],
        loading: false
    };

    parentCall = (record) => {
        this.setState({ record: record, loading: true });
        this.loadProfileSections(record);
    };

    save = () => {
        axios.post('/profile/sections/' + this.state.record.id, this.state.profileSections, {
            withCredentials: true
        }).then((data) => {
            if (data.data.success) {
                notification.info({
                    message: T.translate("config_profiles_update"),
                    description: T.translate("config_profiles_update_success_message")
                });
            }
        });
        this.props.hide();
    };

    loadSections = () => {
        let sections = [];
        this.setState({ allSections: [] });
        for (let i = 0; i < menu.length; i++) {
            let found = false;
            for (let j = 0; j < this.state.profileSections.length; j++) {
                if (menu[i].url == this.state.profileSections[j]) {
                    found = true;
                }
            }
            if (found) {
                sections.push(
                    <Row key={'row' + i}>
                        <Col style={{ margin: '0 0 10px 0' }} key={'col' + i}>
                            <Checkbox key={'chk' + i} value={menu[i].url} onChange={this.check} defaultChecked={true}>{menu[i].name}</Checkbox>
                        </Col>
                    </Row>
                );
            } else {
                sections.push(
                    <Row key={'row' + i}>
                        <Col style={{ margin: '0 0 10px 0' }} key={'col' + i}>
                            <Checkbox key={'chk' + i} value={menu[i].url} onChange={this.check} defaultChecked={false}>{menu[i].name}</Checkbox>
                        </Col>
                    </Row>
                );
            }
        }
        this.setState({ allSections: sections, loading: false });
    };

    loadProfileSections = (record) => {
        axios.get('/profile/sections/' + record.id , {
            withCredentials: true,
        }).then((data) => {
            if(data.data.data){
                let profileSections = [];
                for(let i = 0;i<data.data.data.length;i++){
                    profileSections.push(data.data.data[i].section_id)
                }
                this.setState({ profileSections: profileSections });
                this.loadSections();
            }
        });
    };

    check = (e) => {
        let found = '';
        for (let i = 0; i < this.state.profileSections.length; i++) {
            if (e.target.value == this.state.profileSections[i]) {
                found = e.target.value;
            }
        }
        if (e.target.checked) {
            let addSection = this.state.profileSections;
            if (found == '') {
                addSection.push(e.target.value);
            }
            this.setState({ profileSections: addSection });
        } else {
            let delSection = this.state.profileSections;
            if (found != '') {
                delSection.splice(delSection.indexOf(found), 1);
            }
            this.setState({ profileSections: delSection });
        }
    };

    render() {
        return (
            <Modal
                visible={this.props.show}
                title={T.translate("config_profiles_permissions") + " " + this.props.record.name}
                wrapClassName="vertical-center-modal"
                closable={true}
                maskClosable={false}
                onCancel={this.props.hide}
                footer={[
                    <div key="modalProcSecFoot">
                        <Button key="submit"
                            type="primary"
                            size="large"
                            onClick={this.save}
                            id="add_sections">
                            {T.translate("common_button_ok")}
                        </Button>
                        <Button key="cancel" size="large"
                            onClick={this.props.hide}>
                            {T.translate("common_button_cancel")}
                        </Button>
                    </div>
                ]}
            >
                {this.state.loading ? <Spin/> : this.state.allSections}
            </Modal>
        );
    }
}

export default ProfSecModal;