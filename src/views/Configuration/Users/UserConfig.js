import React, {Component} from "react";
import {notification, Button, Card} from "antd";
import axios from "axios";
import T from "i18n-react/dist/i18n-react";

import GenericAddForm from '../../../components/Form/GenericAddForm'
import TaggedFilter from "../../../components/TaggedFilter/TaggedFilter";
import EditableTable from "../../../components/EditableTable/EditableTable";


export default class UserConfig extends Component {

    constructor(props) {
        super(props);
        this.app = this.props.app;
        this.handleAdd = this.handleAdd.bind(this);
    }

    state = {
        data: [],
        pagination: {},
        loading: false,
        selectedRowKeys: [],
        filters: {}
    };

    columns = [
        {
            title: T.translate('config_users_name'),
            dataIndex: 'username',
            key: 'username',
            width: 200,
            filter: true,
            filterId: 'filterName'
        }, {
            title: T.translate('config_users_password'),
            dataIndex: 'password',
            key: 'password',
            width: 200,
            type: 'password'
        }, {
            title: T.translate('config_users_profiles'),
            dataIndex: 'profiles',
            key: 'profiles',
            values: 'profileids',
            type: 'multiCombo',
            store: '/profile/list',
            filter: true
        }
    ];

    saveRecord = (record) => {
        if(record.username===''){
            notification.error({
                message: T.translate("common_error_update"),
                description: T.translate("config_user_blank_name")
            });
            this.search();
            return;
        }
        if(record.password===''){
            notification.error({
                message: T.translate("common_error_update"),
                description: T.translate("config_user_blank_password")
            });
            this.search();
            return;
        }
        axios.post('/user/' + record.id, record, {
            withCredentials: true
        }).then((data) => {
            if (data.data.success) {
                this.search();
                notification.info({
                    message: T.translate("config_user_update"),
                    description: T.translate("config_user_update_success_message")
                });
            } else {
                this.search();
                switch (data.data.message){
                    case 'Unique constraint error':
                        notification.error({
                            message: T.translate("common_error"),
                            description: T.translate("config_user_unique_error_message")
                        });
                        break;
                    default:
                        notification.error({
                            message: T.translate("common_error_update"),
                            description: T.translate("config_user_update_error_message")
                        });
                }
            }
        })
    };

    addOverContent = () => {
        return (
            <Card title = {T.translate("config_user_add")}>
                <GenericAddForm fields={['username','password']}
                                fieldNames={[T.translate('config_users_name'), T.translate('config_users_password')]}
                                types={['text', 'password']}
                                post={'/user'}
                                required={[true, true]}
                                handleAdd={this.handleAdd}
                                handleCancel={this.handleCancel}
                                layout={'inline'}/>
            </Card>
        )
    };

    handleAdd = (data) => {
        if (data.success) {
            this.search();
            notification.info({
                message: T.translate("config_users_add"),
                description: T.translate("config_user_update_success_message")
            });
            this.setState({visibleForm: false})
        } else {
            switch (data.message){
                case 'Unique constraint error':
                    notification.error({
                        message: T.translate("common_error"),
                        description: T.translate("config_user_unique_error_message")
                    });
                    break;
                default:
                    notification.info({
                        message: T.translate("common_error"),
                        description: T.translate("config_user_add_error_message")
                    });
            }
        }
    };

    handleCancel = () => {
        this.setState({visibleForm: false})
    };

    deleteRecord = (id) => {
        axios.post('/user/delete/'+id, {}, {
            withCredentials: true
        }).then((data) => {
            if (data.data.success) {
                this.search();
                notification.info({
                    message:  T.translate("config_users_del"),
                    description: T.translate("config_user_del_succes_message")
                });
            } else {
                notification.error({
                    message: T.translate("common_error"),
                    description: T.translate("config_user_del_error_message")
                });
            }
        })
    };

    search = (params = {}) => {
        this.setState({
            loading: true,
            data: []
        });
        axios.get('/user/list', {
            withCredentials: true,
            params: params
        }).then((data) => {
            const pagination = {...this.state.pagination};
            // Read total count from server
            pagination.total = data.data.total;
            pagination.showSizeChanger = true;
            this.setState({
                loading: false,
                data: data.data.data,
                pagination,
            });
        });
    };


    componentDidMount = () => {
        this.search();
        this.app.setCurrentBreadcrumbs({
            "ilp": false,
            "configuration": "/configuration",
            "config_users": true//"/configuration/users",
        });

    };

    showForm = () => {
        this.setState({visibleForm: true})
    };

    render() {
        return (
            <div>
                <h2>{T.translate("config_users_title")}</h2>
                <br/>
                <TaggedFilter
                    parent={this}
                    columns={this.columns}
                    fetchFunction={this.search}
                    currentFilters={this.state.filters}
                />
                <Button type="primary" id="btn_add" onClick={this.showForm}>{T.translate("common_button_add")}</Button>
                <br/>
                <br/>
                <EditableTable
                    tableID={'UserConfig'}
                    dataSource={this.state.data}
                    pagination={this.state.pagination}
                    loading={this.state.loading}
                    columns={this.columns}
                    saveRecord={this.saveRecord}
                    deleteRecord={this.deleteRecord}
                    cancelEdit={() => this.search(this.state.filters)}
                />
                {this.state.visibleForm === true && this.addOverContent()}
            </div>
        );
    }
}