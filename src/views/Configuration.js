import React, {Component} from 'react';


import ConfigurationSection from './Configuration/ConfigurationSection';

export default class Configuration extends Component {

    section = [
        {
            key: 'user',
            icon: 'user',
            links: {
                users: '/configuration/users',
                profiles: '/configuration/profiles'
            }
        },
        {
            // key: 'bars',
            // icon: 'bars',
            key: 'exception',
            icon: 'exception',
            links: {
                log: '/configuration/log',
                //simulator: '/configuration/simulator',
                // traceability: '/configuration/traceability'
            }
        }
    ];


    componentDidMount = () => {
        this.props.app.setCurrentBreadcrumbs({
            "ilp": false,
            "configuration": true//"/configuration"
        });
    };

    createSections = () => {

        let html = [];

        for (let i = 0; i < this.section.length; i++)
            html.push(
                <ConfigurationSection
                    key={this.section[i].key}
                    icon={this.section[i].icon}
                    links={this.section[i].links}
                />
            );

        return html;
    };

    render() {
        return (
            <div>
                {this.createSections()}
            </div>
        );
    }
};