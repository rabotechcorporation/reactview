import React, { Component } from 'react';
import T from 'i18n-react'


export default class Unauthorized extends Component {

    render() {

        return (
            <div>
                <h1>{T.translate("common_access_denied")}</h1>
                <p>{T.translate("user_access_denied")}</p>
            </div>

        )
    }
}