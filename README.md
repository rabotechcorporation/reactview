# ILP View

La vista de ILP se ha realizado utilizando:

- ReactJS (create-react-app)
- ant.design


Para ejecutar y configurar el entorno:

- npm install (instalar dependencias)
- npm start (ejecutar en dev)

Para buildear y desplegar el proyecto:

- npm run buid
- copiar la carpeta build en el servidor destino (apache2, el propio ilp, etc)


Para modificar las variables del tema y personalizar la apariencia:

- Modificar el archivo config-overrides.js añadiendo las posibles variables al objeto 'modifyVars'
- Variables disponibles: https://github.com/ant-design/ant-design/blob/master/components/style/themes/default.less

Para debugear, si utilizas vscode con pulsar F5 despues de haberla ejecutado (npm start) se abrirá chrome y parará en los puntos puestos. Para esto hay que instalar la extensión 'Debugger for Chrome' en el vscode. A partir de aqui lo que pase en el navegador saltará en los puntos de interrupción del IDE.

## Descripción Simple

React se basa en componentes, toda la aplicación se divide en pequeños trozos autosuficientes que permiten mantener la aplicación de forma facil. La sintáxis utilizada en React es ES6 que incluye ciertas mejoras con respecto a ES4 (ver docu).

Un componente simple sería:

```javascript
import React, { Component } from 'react';



export default class Hello extends Component {
    render() {
        return (
            <div>
                Hello World
            </div>
        );
    }
}
```

Como se ve en el ejemplo lo primero es importar, de igual forma que en Java ES6 gestiona las dependencias entre clases mediante imports. En este caso se importa :

- React: la librería completa para poder utilizar el lenguaje JSX 
- {Component}: Mediante esta sintáxis se hace referencia a un único módulo dentro del paquete elegido (Sería lo mismo que hacer ```const Component = React.Component```)

El siguiente punto es definir el objeto que se exportará a los elementos en cuanto importen este componente. Para esto se define:

```javascript
//Ejemplo ES6
export default class Hello extends Component {
    prop1 = "foo"
}

//A modo de explicación en nodejs (ES4) esto se haría
var Hello = {
    prop1: "foo"
};
module.exports = Hello;
```

Una clase no tiene porque exportar un único componente, es posible definir múltiples componentes dentro de una sola clase e importarlos mediante la sitáxis definida anteriormente ({ComponentName})
```javascript
export class Hello extends Component {
    prop1 = "foo"
}
export class Hello2 extends Component {
    prop2 = "foo2"
}
```

Todas los los Component disponen de un método ```render()``` este método será llamado por el componente padre para pintarlo por tanto es necesario que retorne su contenido.
Este contenido se define en JSX. Este lenguaje es una mezcla entre HTML y XML con inyección JS y permite hacer cosas como:

```javascript
render() {
    return (

        <Modal
            visible={this.props.modalVisible === true ? "visible" : "hidden"}
            title="Title"
            onOk={this.props.handleOk}
            onCancel={this.props.handleCancel}
            {...this.state.configsVarias}
            footer={[
                <Button key="back" size="large" onClick={(e) => { console.log(e) }}>Return</Button>,
                <Button key="submit" type="primary" size="large" loading={this.props.loading} onClick={this.props.handleOk}>
                    Submit
                </Button>,
            ]}
        >
            <p>Some contents...</p>
            <p>Some contents...</p>
            <p>Some contents...</p>
            <p>Some contents...</p>
            <p>Some contents...</p>
        </Modal>
    );
}
```

El contenido de JSX puede representarse y manejarse de dos formas. 
- De forma directa como en el ejemplo anterior
- Mediante un array:
```javascript
draw = () => {
    let dataArr = [];
    dataArr.push(<p>Cosa1</p>);
    dataArr.push(<p>Cosa2</p>);

    return dataArr;
}

```

Cada componente dispone de dos propiedades principales y heredadas de forma automática:

- props: configuración con la que se ha registrado el componente ``` <Componente1 prop1="valor" />```
- state: datos del estado actual del componente que permiten reflejar cambios en tiempo real (similar al ViewModel de ExtJS)



## Traducciones:

Para usar las traducciones en una clase / componente es necesario:

- Importar lo siguiente:
```javascript
import { intlShape, FormattedMessage, defineMessages } from 'react-intl';
```
- Añadir al final del archivo
```javascript
//Cambiar el nombre de la clase
Class.contextTypes = {
    intl: intlShape
};
```
- Definir los mensajes a utilizar. Mapear desde los mensajes definidos en el archivo json hasta lo que dispondrá la vista para su uso:

```javascript
const messages = defineMessages({
    logoutTitle: {
        id: 'Logout.modal.title',
        defaultMessage: 'Cerrar Sesión',
    }
});
```
Hay un ejemplo real en el archivo AppHeader.js

Enlaces de interes:

https://ant.design/docs/react/use-with-create-react-app
https://reactjs.org/docs/installation.html