const { injectBabelPlugin } = require('react-app-rewired');
const rewireLess = require('react-app-rewire-less');


module.exports = function override(config, env) {

    config = injectBabelPlugin(['import', { libraryName: 'antd', style: true }], config);
    config = rewireLess.withLoaderOptions({
        javascriptEnabled: true,
        modifyVars: {
            '@primary-color': '#0077a1',
            '@link-color': '#0077a1',
            '@font-size-base': '11px',
            '@icon-url': '"../../../../../public/iconfont/iconfont"',
        },
    })(config, env);

    return config;
};